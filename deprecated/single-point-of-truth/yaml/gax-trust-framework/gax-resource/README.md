# The _gax-resource_ Folder
This folder defines the top-level class _Resource_ and contains subclasses of _Resource_ to describe artefacts in the Trust Framework with resource character.

## Infrastructure Grouping and Geographic Location Resources

### AvailabilityZone

The _AvailabilityZone_ class describes the aggregation of individual resources that are typically co-located or co-associated to a fire-protection compartment of a data center. It is itself a subclass of _Resource_ and is typically used to group those resources that are producing a service in a non-redundant fashion. 

| Attribute              | Card. | Comment                                         |
|------------------------|-------|-------------------------------------------------|
| `aggregationOf`        | 0..*  | References to physical and abstract resources associated with this _AvailabilityZone_. | 


### DataCenter

The _DataCenter_ class describes the aggregation of _AvailabilityZone_ instances making up such data center. It is itself a subclass of _PhysicalResource_ and thus inherits its geographic location attributes _locationAddress_ and _locationGPS_

| Attribute              | Card. | Comment                                         |
|------------------------|-------|-------------------------------------------------|
| `aggregationOf`        | 1..*  | References to _AvailabilityZones_ instances making up this _DataCenter_ instance. | 

### Region

The _Region_ class describes the aggregation of _DataCenter_ instances into a larger geographic grouping. It is itself a subclass of _Resource_ and is typically used to group those _DataCenter_ instances that are producing services in a redundant fashion. 

| Attribute              | Card. | Comment                                         |
|------------------------|-------|-------------------------------------------------|
| `aggregationOf`        | 1..*  | References to _DataCenter_ instances making up this _Region_ instance. | 


## CodeArtifact and Image

_CodeArtifact_ describes any software piece that can be executed by a _Compute_ service. It is a subclass of _SoftwareResource_. 

| Attribute              | Card.  | Comment                                         |
|------------------------|-------|-------------------------------------------------|
| `name`        | 1..*   | Name identifying the code artifact. | 

An _Image_ instance is a subclass of a _CodeArtifact_ resource that exists in binary form and can be stored. 

| Attribute              | Card. | Comment                                         |
|------------------------|-------|-------------------------------------------------|
| `diskFormat`        | 0..1   | Disk format. Default 'RAW'. | 
| `checkSum`        | 0..1   | Checksum details of the Image. A structured object of type gax-trust-framework:Checksum. | 
| `version`        | 0..1   | Version of this image. | 
| `cpuReq`        | 1..1   | CPU requirements for this image. A structure object of type CPU. | 
| `fileSize`        | 0..1   | File size of image. A structured object of type measure, e.g. measure:value=24 and measure:unit=GB. | 
| `ramSize`        | 0..1   | Size of image when loaded into RAM. A structured object of type measure, e.g. measure:value=24 and measure:unit=GB. | 
| `encryption`        | 0..1   | Details of image ecryption. A structured object of type gax-trust-framework:Encryption. | 
| `signature`        | 0..1   | Details of image signature. A structured object of type gax-trust-framework:Signature. | 

### PxeImage

_PxeImage_ describes an image variant to be loaded by a physical server over the network. It is a subclass of _Image_. 

| Attribute              | Card.  | Comment                                         |
|------------------------|-------|-------------------------------------------------|
| `format`        | 1..1  | PXE image format for a bare-metal server, e.g. 'iso', 'winpe'. | 

### VmImage

_VmImage_ describes an image variant to be loaded by a hypervisor after instantiation of a virtual machine. It is a subclass of _Image_. 

| Attribute              | Card.  | Comment                                         |
|------------------------|-------|-------------------------------------------------|
| `format`        | 1..1   | Image format for a virtual machine server, e.g. 'qcow2', 'vhd', 'vhdx', 'vmdk', 'vdi', 'iso', 'ovf', 'ova'. | 

### ContainerImage

_ContainerImage_ describes an image variant to be loaded by a container runtime. It is a subclass of _Image_. 

| Attribute              | Card. | Comment                                         |
|------------------------|-------|-------------------------------------------------|
| `baseOS`        | 1..1   | Container image base operating system, e.g. 'linux', 'windows'. | 
| `format`        | 1..1   | container image format, e.g. 'docker v1', 'docker v2', 'oci', 'lxc'. | 

### ComputeFunctionCode

_ComputeFunctionCode_ describes the high-level language file that will be executed by a Compute Function Service. . It is a subclass of _CodeArtifact_. 

| Attribute              | Card. | Comment                                         |
|------------------------|-------|-------------------------------------------------|
| `file`        | 1..1  | URL referring to function code file. | 



