# The _gax-trust-framework_ Folder
This file hierarchy contains classes belonging to the Gaia-X Trust Framework as described in the Gaia-X Trust Framework Document at https://docs.gaia-x.eu/policy-rules-committee/trust-framework/latest/.

# Infrastructure Service Characteristics Overview

Infrastructure Services are made up from Compute, Storage, and Connectivity Services.

```mermaid
classDiagram

class ServiceOffering {
    <<abstract>>
}

class InfrastructureServiceOffering {
    <<abstract>>
}

ServiceOffering <|-- InfrastructureServiceOffering

class Compute {
    <<abstract>>
}

class Storage {
    <<abstract>>
}

class Connectivity {
    <<abstract>>
}

InfrastructureServiceOffering <|-- Compute
InfrastructureServiceOffering <|-- Storage
InfrastructureServiceOffering <|-- Connectivity
```

# Introduced Conceptual Model Terminology

## Service Locations

Any service is produced in availability zones of a provider's infrastructure. Such availability zones are aggregated into data centers which in turn form a provider's covered regions. 

```mermaid
classDiagram

region --> "1..*" dataCenter:aggregationOf

Resource <|-- region
Resource <|-- PhysicalResource 
Resource <|-- availabilityZone

PhysicalResource <|-- dataCenter

dataCenter --> "1..*" availabilityZone:aggregationOf
availabilityZone -->"0..*" Resource:aggregationOf


serviceOffering --> "1..*" availabilityZone:hostedOn
```


For a detailed description of availabilityZone, dataCenter, and region, please see [here](https://gitlab.com/gaia-x/technical-committee/service-characteristics/-/blob/develop/single-point-of-truth/yaml/gax-trust-framework/gax-resource/README.md).


# Infrastructure Service Characteristics 
## Storage 

Storage infrastructure services are composed of Object Storage Services (exposing unstructured objects, blobs or buckets), Block Storage Services (exposing fixed blocks between certain "extents") and File Storage Services( exposing files). 


```mermaid
classDiagram

class Storage {
    <<abstract>>
}


class Object Storage {
     

}


class File Storage {
    
}


class Block Storage {
     

}

Storage --> Object Storage
Storage --> File Storage
Storage --> Block Storage
```

Storage Services expose attributes common to Block, File and Object as given below: 

| Attribute              | Card. | Comment                                         |
|------------------------|-------|-------------------------------------------------|
| `usableSize`        | 0..*  | Usable Capacity in TBs (Block, File or Object) | 
|   `totalSize`  | 0..*  | Total Capacity in TBs (Block, File or Object). Total capacity includes space to manage redundancy etc|
| `compressionAlgorithm`      | 0..* | Algorithm used in compressing data in storage | 
| `hasBackup`        |0..* | Backup Strategies within Storage as a Service | 
|   `hasSnapshot`  | 0..* |Snapshot Strategies within Storage as a Service |
| `hasLifetimeManagement`      | 0..* | Lifetime of Data in this storage service since last access with data moving externally after this| 
| `hasReplication`        | 0..*  | Data replication strategies for this storage service | 
|   `hasQoS`  |0..* | (Reference to Quality of Service attributes for this storage service: Not all QoS attributes may apply to every Service Offering, but for now we keep it simple and do not constrain the usage of the QoS attributes)|
| `accessAttributes`      | 0..* | How the data is accessed, read only, read-write etc | 
| `deduplicationSupport`        | 0..* |Deduplication is a method of storing a reference that points to multiple similar blocks hence saving space. https://en.wikipedia.org/wiki/Data_deduplication | 
|   `deduplicationType`  | 0..*  | There are different types of Deduplication|
| `accessSemantics`      | 0..*  |Does I/O conform to the POSIX standard? POSIX:https://en.wikipedia.org/wiki/POSIX. https://www.pdl.cmu.edu/posix/docs/POSIX-extensions-goals.pdf, https://www.nextplatform.com/2017/09/11/whats-bad-posix-io/ | 
| `encryption`        | 0..*  |  Information about used encryption techniques for data storage| 
|   `versioning`  | 0..* | Get Support for File, Block or Object versioning |
| `storageConsistency`      |0..* | Type/level of consistency provided, Ref- https://en.wikipedia.org/wiki/Consistency_model | 
| `dataViews`        |0..* | Capability to compose logical data views - Present the subset of the data as a logical data entity | 
|   `multipleViews`  | 0..* |Capability for multiple views. For example is it possible to view Objects as Files, or the other way around? |


Attributes specific to Object Storage is as given below:

| Attribute              | Card. | Comment                                         |
|------------------------|-------|-------------------------------------------------|
| `objectRedundancyMechanism`        | 0..*  | Underlying data protection mechanism. Some example are - Triplication creates three copies in three different protection domains. Some references are here - https://en.wikipedia.org/wiki/Erasure_code, https://en.wikipedia.org/wiki/RAID | 
|   `objectAPICompatibility`  | 0..*  | Compatibility for third party object storage  APIs. Ref:https://docs.aws.amazon.com/AmazonS3/latest/API/Type_API_Reference.html, https://docs.microsoft.com/en-us/rest/api/storageservices/blob-service-rest-api |

Attributes specific to Block Storage is as given below:
| Attribute              | Card.  | Comment                                         |
|------------------------|-------|-------------------------------------------------|
| `blockSize`        | 0..* | Block size of block, file or object store to be used | 
|   `blockRedundancyMechanism`  | 0..* |  Underlying data protection mechanism to be employed. Some example are - Triplication creates three copies in three different protection domains. Some references are here - https://en.wikipedia.org/wiki/Erasure_code, https://en.wikipedia.org/wiki/RAID|
| `blockStorageTechnology'`      | 0..* | Underlying storage technology type to be used| 
| `lowLevelBlockAccessProtocol`        |0..* | Underlying low level access protocol for the storage service to be used. These are various methods to access the storage technology. Some examples anbd references - https://www.enterprisestorageforum.com/hardware/nvme-vs-sata-comparing-storage-technologies/ | 


Attributes specific to File Storage is as given below:
| Attribute              | Card.  | Comment                                         |
|------------------------|-------|-------------------------------------------------|
| `fileSystemType`        | 0..* | Filesystem Type for storage partition Ref:https://opensource.com/article/18/4/ext4-filesystem| 
|   `fileBlockSize`  | 0..* | Block size of block, file or object store |
| `fileRedundancyMechanism`      | 0..* |Underlying data protection mechanism. Some example are - Triplication creates three copies in three different protection domains. Some references are here - https://en.wikipedia.org/wiki/Erasure_code, https://en.wikipedia.org/wiki/RAID | 
| `highLevelAccessProtocol`        |0..* | Underlying higher level access protocol, These mainly refer to access methods such as NFS (Network File System), Common Internet File System (CIFS) and Hadoop File System (HDFS) | 



## Compute

Compute Infrastructure services are made out of BareMetal, VirtualMachine, Container, and ComputeFunction services. VirtualMachine and Container also exist in autoscaled versions. 


```mermaid 
classDiagram

class Compute {
    <<abstract>>
}


class BareMetal 
class VirtualMachine
class AutoscaledVirtualMachine
class Container
class AutoscaledContainer
class ComputeFunction


Compute <|-- BareMetal 
Compute <|-- VirtualMachine 
VirtualMachine <|-- AutoscaledVirtualMachine
Compute <|-- Container
Container <|-- AutoscaledContainer
Compute <|-- ComputeFunction
```

Each Compute infrastructure service references two important other classes, namely InstatiationRequirements (which are the available configurations of such service), and the available CodeArtifacts, i.e. the SoftwareResources available to be run on such Compute infrastructure service: 

```mermaid
classDiagram

class Compute {
    <<abstract>>
    tenantSeparation
}

class Configuration {
    <<abstract>>
}

class SoftwareResource

class InstantiationRequirements {
    <<abstract>>
}

class CodeArtifact {
    <<abstract>>
}

Compute --> InstantiationRequirements:1..*
Compute --> CodeArtifact:1..*

InstantiationRequirements <|-- Configuration
CodeArtifact <|-- SoftwareResource
```


| Attribute              | Card. | Comment                                         |
|------------------------|-------|-------------------------------------------------|
| `codeArtifact`        | 1..*  | A structured object of type gax-trust-framework:CodeArtifact | 
| `instantiationReq`    | 1..*  | A structured object of type gax-trust-framework:InstantiationRequirements |
| `tenantSeparation`      | 0..1 | How compute resources of different tenants are separated. Default value = hw-virtualized | 


### Image

```mermaid
classDiagram

CodeArtifact <|-- Image
```

The _Image_ class is a subclass of _CodeArtifact_ when a software resource with binary requirements is needed. For a more detailed description of _Image_ please see [here](https://gitlab.com/gaia-x/technical-committee/service-characteristics/-/tree/develop/single-point-of-truth/yaml/gax-trust-framework/gax-resource).


### Baremetal Compute

```mermaid
classDiagram

class BareMetal 

InstantiationRequirements <|-- ServerFlavor

BareMetal --> ServerFlavor:1..*
BareMetal --> PxeImage:0..*

Image <|-- PxeImage
```

For a detailed description of ServerFlavor, see [here](https://gitlab.com/gaia-x/technical-committee/service-characteristics/-/blob/develop/single-point-of-truth/yaml/gax-trust-framework/gax-configuration/README.md).

For a detailed description of PxeImage, see [here](https://gitlab.com/gaia-x/technical-committee/service-characteristics/-/blob/develop/single-point-of-truth/yaml/gax-trust-framework/gax-resource/README.md).

| Attribute              | Card. | Comment                                         |
|------------------------|-------|-------------------------------------------------|
| `codeArtifact`        | 0..*  | A structured object of type gax-trust-framework:a structured object of type gax-trust-framework:PxeImage | 
| `instantiationReq`    | 1..*  | A structured object of type gax-trust-framework:ServerFlavor |


### Virtual Machine Compute

```mermaid
classDiagram

class VirtualMachine 

InstantiationRequirements <|-- ServerFlavor

VirtualMachine --> ServerFlavor:1..*
VirtualMachine --> VmImage:1..*

Image <|-- VmImage
```


For a detailed description of ServerFlavor, see [here](https://gitlab.com/gaia-x/technical-committee/service-characteristics/-/blob/develop/single-point-of-truth/yaml/gax-trust-framework/gax-configuration/README.md).

For a detailed description of VmImage, see [here](https://gitlab.com/gaia-x/technical-committee/service-characteristics/-/blob/develop/single-point-of-truth/yaml/gax-trust-framework/gax-resource/README.md).

| Attribute              | Card. | Comment                                         |
|------------------------|-------|-------------------------------------------------|
| `codeArtifact`        | 1..*  | A structured object of type gax-trust-framework:a structured object of type gax-trust-framework:VmImage | 
| `instantiationReq`    | 1..*  | A structured object of type gax-trust-framework:ServerFlavor |


#### Autoscaled VM Compute

For autoscaling, additional configuration in the form of an autoscaling specification: 

```mermaid
classDiagram

class VirtualMachine

class AutoscaledVirtualMachine

VirtualMachine <|-- AutoscaledVirtualMachine
AutoscaledVirtualMachine --> autoscaledVmServiceSpec:1..*

VmAutoscalingGroupSpec <|-- autoscaledVmServiceSpec
InstantiationRequirements <|-- VmAutoscalingGroupSpec
```


For a detailed description of VmAutoscalingGroupSpec, see [here](https://gitlab.com/gaia-x/technical-committee/service-characteristics/-/blob/develop/single-point-of-truth/yaml/gax-trust-framework/gax-configuration/README.md).

| Attribute              | Card. | Comment                                         |
|------------------------|-------|-------------------------------------------------|
|  `autoscaledVmServiceSpec`| 1..* | A structured object of type gax-trust-framework:VMAutoScalingGroupSpec | 



### Container Compute

```mermaid
classDiagram

class Container 

InstantiationRequirements <|-- ContainerResourceLimits

Container --> ContainerResourceLimits:1..*
Container --> ContainerImage:1..*

Image <|-- ContainerImage
```

For a detailed description of ContainerResourceLimits, see [here](https://gitlab.com/gaia-x/technical-committee/service-characteristics/-/blob/develop/single-point-of-truth/yaml/gax-trust-framework/gax-configuration/README.md).

For a detailed description of ContainerImage, see [here](https://gitlab.com/gaia-x/technical-committee/service-characteristics/-/blob/develop/single-point-of-truth/yaml/gax-trust-framework/gax-resource/README.md).

| Attribute              | Card. | Comment                                         |
|------------------------|-------|-------------------------------------------------|
| `codeArtifact`        | 1..*  | A structured object of type gax-trust-framework:a structured object of type gax-trust-framework:ContainerImage | 
| `instantiationReq`    | 1..*  | A structured object of type gax-trust-framework:ContainerResourceLimits |

_add Autoscaled Container_

### Compute Functions

```mermaid
classDiagram

class ComputeFunction 

InstantiationRequirements <|-- ComputeFunctionTrigger

ComputeFunction --> ComputeFunctionTrigger:1..*
ComputeFunction --> ComputeFunctionCode:1..*

CodeArtifact <|-- ComputeFunctionCode
```


For a detailed description of ComputeFunctionTrigger, see [here](https://gitlab.com/gaia-x/technical-committee/service-characteristics/-/blob/develop/single-point-of-truth/yaml/gax-trust-framework/gax-configuration/README.md).

For a detailed description of ComputeFunctionCode, see [here](https://gitlab.com/gaia-x/technical-committee/service-characteristics/-/blob/develop/single-point-of-truth/yaml/gax-trust-framework/gax-resource/README.md).

| Attribute              | Card. | Comment                                         |
|------------------------|-------|-------------------------------------------------|
| `codeArtifact`        | 1..*  | A structured object of type gax-trust-framework:a structured object of type gax-trust-framework:ComputeFunctionCode | 
| `instantiationReq`    | 1..*  | A structured object of type gax-trust-framework:ComputeFunctionTrigger |

## Connectivity 

Connectivity Infrastructure services are made out of Physical, Link and Network services. Interconnection services between providers exist as a subclass of Network services. 

Following the classification of OSI Model we have introduced and modeled Layer 1-3 services. 

```mermaid
classDiagram
    class InfrastructureServiceOffering {
        <<abstract>>
    }

    class ConnectivityServiceOffering {
        <<abstract>>
    }

    class connectivityConfiguration {
        sourceIdentifierA 
        destinationIdentifierZ
    }

    class Configuration {
        <<abstract>>
    }

    class InstantiationRequirements {
        <<abstract>>
    }


    Configuration <|-- InstantiationRequirements
    InstantiationRequirements <|-- connectivityConfiguration
    
    InfrastructureServiceOffering <|-- ConnectivityServiceOffering

    ConnectivityServiceOffering <|-- NetworkConnectivityServiceOffering
    ConnectivityServiceOffering <|-- LinkConnectivityServiceOffering
    ConnectivityServiceOffering <|-- PhysicalConnectivityServiceOffering

    NetworkConnectivityServiceOffering <|--InterconnectionServiceOffering

    InterconnectionServiceOffering --> connectivityConfiguration
    NetworkConnectivityServiceOffering --> connectivityConfiguration
    LinkConnectivityServiceOffering --> connectivityConfiguration
    PhysicalConnectivityServiceOffering --> connectivityConfiguration

    connectivityConfiguration --> "0..1" InterconnectionPointIdentifier
    connectivityConfiguration --> "0..1" InterconnectionPointIdentifier
```

### Physical

| Attribute              | Card. | Comment                                         |
|------------------------|-------|-------------------------------------------------|
| `circuitType`        | 1..1  | Type of access medium: wired medium access or wireless medium access | 
| `interfaceType`    | 1..1  | For the chosen circuit type, one should know the interface type in case the interoperability is required |
| `connectivityConfiguration`      | 1..* | All possible provided connectivity parameters for this particulr connectivity service offering|

### Link 

| Attribute              | Card. | Comment                                         |
|------------------------|-------|-------------------------------------------------|
| `connectivityConfiguration`      | 1..* | All possible provided connectivity parameters for this particulr connectivity service offering|
| `bandwidth`        | 0..1  | Contractual bandwidth defined in the service level agreement (SLA) | 
| `roundTripTime`    | 0..1  | Contractual latency defined in the SLA |
| `availability`    | 0..1  | Contractual availability of connection defined in the SLA agreement. Availability is measured in the pseudo-unit "percent" |
| `packetLoss`    | 0..1  | Contractual packet loss of connection defined in the SLA agreement. If not specified, then best effort is assumed. PackageLoss s measured in the pseudo-unit "percent" |
| `jitter`    | 0..1  | Contractual jitter defined in the SLA. If not specified, then best effort is assumed. |
| `protocolType`    | 1..1  | Link protocol type |
| `vlanType`    | 0..1  | the chosen types of vlan types |
| `vlanTag`    | 0..1  | Vlan Tag ID that range between 1 and 4094. In case qinq connection type is chosen tow vlan tag, namely outer and innter should be provided|
| `vlanEtherType`    | 0..1  | The ethertype of the vlan in hexadecimal notation. |


### Network

| Attribute              | Card. | Comment                                         |
|------------------------|-------|-------------------------------------------------|
| `connectivityConfiguration`      | 1..* | all possible provided connectivity parameters for this particulr connectivity service offering | 

By convention, an instantiated interconnection service is said to be hosted on a PhysicalResource of the initiating provider, i.e. at the source interconnection point. In service credential terms this means that _hostedOn_ of the sourceIdentifierA and the _hostedOn_ attribute of the _ServiceInstance_ need to be identical.

## connectivityConfiguration

The _connectivityConfiguration_ class encapsulates the configurable attributes of a _ConnectivityServiceOffering_. 

| Attribute              | Card. | Comment                                         |
|------------------------|-------|-------------------------------------------------|
| `sourceIdentifierA`      | 0..1 | InterconnectionPointIdentifier reference of the source service access point | 
| `destinationIdentifierA`      | 0..1 | InterconnectionPointIdentifier reference of the destination service access point | 

## Interconnection Point Identifier

To allow multiple data sources and infrastructures to connect, GAIA-X introduces the concept of the Interconnection Point Identifier (IPI). An IPI is a specific [Service Access Point](https://en.wikipedia.org/wiki/Service_Access_Point) that identifies where ressources can interconnect. It needs to be:

- resolvable within the GAIA-X Conceptual model
- hierachical
- support multiple OSI-Layers and technologies (both physical and virtual)
- be globally unique

An IPI would either point to a physical ressource or reference an instantiated virtual ressource. A Connection between two IPIs is offered by a Network Connectivity service. As those Network Connectivity Services follow the OSI-Model, IPIs also need to be able to reference interconnection points of

*   **Physical Connection Services** (Layer 1)
    
*   **Link Connection Services** (Layer 2)
    
*   **Network Connection Services** (Layer 3)
    
*   and potential other, higher layer interconnection services, such as **Application Layer** etc., i.e. when conneting to an URI
    

In addition, to follow the Trust Framework approach, each IPI has at least to reference:

*   The **geographical location** of the IPI, although a concept of an “International” or “Public” Location should be introduced to allow “floating” IP Blocks or global Satellite Networks. The geographical location should follow the [ISO 3166 country codes](https://en.wikipedia.org/wiki/List_of_ISO_3166_country_codes) or other GAIA-X wide applicable location label and should mainly be used as a reference for the jurisdiction of a given IPI. The geographical location should always reference the jurisdiction in which the Network Connectivity Service originates and is ordered from (the “A-End”) or the jurisdiction the service as a whole is governed under.
    
*   The **Provider** of the IPI, which provides the IPI, but not necessary _owns_ it. For example, the _provider_ of an IP Number is the [IANA](https://iana.org/), but the owner can be the GAIA-X user that has subscribed this IP from his Network Provider. The provider referenced can be a validated GAIA-X Provider, so that his services can be found in the respective federated cataloque, but as described above, also other providers outside of the GAIA-X ecosystem should be able to be reference, such as IANA, to allow for “outside ressource” references
    

As an IPI can specify the Interconnection point of a **Network Connectivity Service Offering** or an **Instantiated Network Connectivity Ressource** it needs to have additionally **Type or Provider Specific Parameters** to be able to describe from where to where a given Network Connectivity Service Offering is connecting to and from (the potential _A- and Z-Ends_), that can be less specific than the designations of the instantiated Network Connectivity ressources. As an example there can be a Network Connectivity Service Offering that offers connectivity to the AWS Direct Connect Product, thus offering an interconnection to AWS Services, while the instantiated ressource also references the actual AWS Tenant or VPC that is connected.  
The later parameters are part of the **Instantiation Requirements**.

### Structure of an IPI

As the IPI needs to be globally unique, hierachical and resolvable it needs to follow a clear structure. The different layers of information are seperated by colons which forms a structure like this:

```java
<Type of IPI>:<Location>:<Provider>:<Type/Provider Specific Parameters>
```

All Type or Provider specific parameters are again separated by colons.

The table below shows some examples of IPIs.

| **Service Example Referenced**  | **Ressource or Instantiated Ressource** | **Type of IPI** | **Jurisdiction** | **Provider as GAIA\-X Participant** | **Type or Provider Specific Parameters**    | **Explanation**                                                                                                                                  | **Completed IPI**                                       |
|---------------------------------|-----------------------------------------|-----------------|------------------|-------------------------------------|---------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------|
| Physical Fibre CrossConnect     | R                                       | Physical        | DE               | <did:90101581255>                   | FR2:0G:0FM102:PP102:14&amp;16               | Data Center \(i\.e\. “FR2\)Floor \(i\.e\. “0G”\)Rack Number \(i\.e\. “0FM102”\)Patch Panel \(i\.e\. “PP102”\)Port Numbers \(i\.e\. “14&amp;16”\) | Physical:DE:EQIX:FR2:0G:0FM102:PP102:14&amp;16          |
| Physical Fibre CrossConnect     | R                                       | Link            | DE               | <did:90101581255>                   | FR2                                         | Data Center \(i\.e\. “FR2”\)                                                                                                                     | Physical:DE:EQIX:FR2                                    |
| Virtual Ethernet Fabric Circuit | R                                       | Link            | DE               | <did:90101581255>                   | EF:AWSDC:FR                                 | Product Offering \(“Equinix Fabric”\)Service Profile \(“AWS Direct Connect”\)Location the Service is offered in \(“Frankfurt”\)                  | Link:DE:EQIX:EF:AWSDC:FR                                |
| Public IP Adress                | I                                       | Network         | Global           | IANA                                | 18\.125\.10\.15                             | Public IP Number as Specified in RFC 791                                                                                                         | Network:Global:IANA:18\.125\.10\.15                     |
| Database Service                | R                                       | URI1            | US               | <did:901223281255>                  | https://dbsaas\.oracle\.com/api/1\.0/access | URL on the public internet as specified in RF 3986                                                                                               | URI:US:ORCL:https://dbsaas\.oracle\.com/api/1\.0/access |


1 Potential Extension to the IPI Types

 IPI is defined via following attributes:

| Attribute              | Card. | Comment                                         |
|------------------------|-------|-------------------------------------------------|
| `dataCenterName`      | 0..1 | the name or Id of datacenter where the service can be accessed | 
| `dataCenterFloor`      | 0..1 |the floor number of datacenter where the service can be accessed|
| `dataCenterRackNumber`      | 0..1 |the Id of datacenter rack number where the service can be accessed|
| `dataCenterPatchPanel`      | 0..1 |the Id of datacenter patch panel where the service can be accessed|
| `dataCenterPortNumber`      | 0..1 |the port number on the patch panel where the service can be accessed|
| `macAddress`      | 0..1 |the mac address required for L2 connectivity setup|
| `ipAddress`      | 0..1 |the IP address required for L3 connectivity setup|

