# The _gax-configuration_ Folder
This folder describes _Configuration_ artefacts as defined in the Gaia-X Conceptual Model, described in the _gax-core_ folder hierarchy. 
_Configuration_ refers to a set of parameters or attributes used during realization or instantiation of services and resources. 

## InstantiationRequirements

_InstantiationRequirements_ is an abstract class representing those attributes that are configurable on service instantiation. 

### ServerFlavor

The _ServerFlavor_ class describes the instantiation requirements for a baremetal or virtual server. 

| Attribute              | Card. | Comment                                         |
|------------------------|-------|-------------------------------------------------|
| `cpuReq`        | 1..1  | CPU requirements. A structured object of type CPU. | 
| `memoryReq`        | 1..1 | Server memory requirements. A structured object of type Memory. | 
| `gpuReq`        | 0..*  | GPU Requirements. A structured object of type GPU. | 
| `networkReq`        | 1..*  | Networking requirements. A structured object of type Network. | 
| `bootVolumeReq`        | 1..1  | Boot volume requirement. A structured object of type Disk. | 
| `additionalVolumeReq`        | 0..*  | Additional volume requirements. A structured object of type Disk. | 
| `isConfidential`        | 1..1  | Boolean value that indicates whether server is of confidential nature. | 
| `confidentialComputingTechnology`        | 0..1  | one ore more particular CC technologies used by this flavor, e.g. 'AMD SEV', 'AMD SEV-ES', 'AMD SEV-SNP', 'ARM TrustZone', 'ARM RME', 'Huawei Qingtian', 'Intel SGX1', 'Intel SGX2', 'Intel TDX'. | 
| `attestationServiceURI`        | 0..1  | Lists the URI of the RESTful API of the associated attestation service. | 


### ContainerResourceLimits

The _ContainerResourceLimits_ class describes the instantiation requirements for an individual container. 

| Attribute              | Card. | Comment                                         |
|------------------------|-------|-------------------------------------------------|
| `cpuReq`        | 0..1  | CPU requirements. A structured object of type CPU. | 
| `numberOfCoresLimit`  | 0..1  | CPU requirements. A structured object of type CPU. | 
| `memoryReq`        | 0..1  | Container min memory requirements. A structured object of type Memory. | 
| `memoryLimit`        | 0..1  | Container memory limit. A structured object of type Memory. | 
| `gpuReq`        | 0..1  | GPU requirements. A structured object of type GPU. | 
| `gpuLimit`        | 0..1  | GPU limit. Maximum number of GPUs accessible by this container. | 
| `isConfidential`        | 1..1  | Boolean value that indicates whether server is of confidential nature. | 
| `confidentialComputingTechnology`        | 0..1  | One ore more particular CC technologies used by this flavor, e.g. 'AMD SEV', 'AMD SEV-ES', 'AMD SEV-SNP', 'ARM TrustZone', 'ARM RME', 'Huawei Qingtian', 'Intel SGX1', 'Intel SGX2', 'Intel TDX'. | 
| `attestationServiceURI`        | 0..1  | Lists the URI of the RESTful API of the associated attestation service. | 

### ComputeFunctionTrigger

The _ComputeFunctionTrigger_ class describes the instantiation requirements for a compute function. 

| Attribute              | Card. | Comment                                         |
|------------------------|-------|-------------------------------------------------|
| `URL`        | 0..1  | URI / URL used to trigger this compute function. | 
| `isConfidential`        | 1..1  | Boolean value that indicates whether server is of confidential nature. | 
| `confidentialComputingTechnology`        | 0..1  | One ore more particular CC technologies used by this flavor, e.g. 'AMD SEV', 'AMD SEV-ES', 'AMD SEV-SNP', 'ARM TrustZone', 'ARM RME', 'Huawei Qingtian', 'Intel SGX1', 'Intel SGX2', 'Intel TDX'. | 
| `attestationServiceURI`        | 0..1  | Lists the URI of the RESTful API of the associated attestation service. | 


### VmAutocalingGroupSpec

The _VmAutoscalingGroupSpec_ class hosts the additional attributes required to describe the group of server nodes and their scaling conditions in the AutoscaledVirtualMachine service. 

| Attribute              | Card. | Comment                                         |
|------------------------|-------|-------------------------------------------------|
| `minInstantiatedVM`        | 0..1  | Minimim number of VM instances in the AutoscalingGrop. | 
| `maxInstantiatedVM`        | 0..1  | Maximum number of VM instances in the AutoscalingGrop. | 
| `loadMetric`  | 0..1  | Load Metric type, e.g. 'cpu load', 'memory load', 'IO load', 'cost' ]
| `loadMetricTarget`        | 0..1  | Per VM instance Load Metric Target value. Must be defined if LoadMetric is defined. Must be between Min and Max. | 
| `loadMetricTargetMin`        | 0..1  | 'Per VM instance Load Metric Target value. Must be defined if LoadMetric is defined. Must be < Max'. | 
| `loadMetricTargetMax`        | 0..1  | Per VM instance Load Metric Target value. Must be defined if LoadMetric is defined. Must be > min'. | 
| `scalingPlan`        | 0..1  |  GPU limit. Maximum number of GPUs accessible by this container. | 
