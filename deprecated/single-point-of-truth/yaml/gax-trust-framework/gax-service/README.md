# The _gax_service_ Folder
This folder defines the top-level class _ServiceOffering_ and contains subclasses of _ServiceOffering_ to describe artefacts in the Trust Framework with service character.

# Infrastructure Service Characteristics 

Infrastructure Services are made up from Compute, Storage, and Connectivity Services.

```mermaid
classDiagram

class ServiceOffering {
    <<abstract>>
}

class InfrastructureOffering {
    <<abstract>>
}

class Compute {
    <<abstract>>
    
}

class Storage {
    <<abstract>>
}

class Connectivity {
    <<abstract>>
}

ServiceOffering <|-- InfrastructureOffering 
InfrastructureOffering <|-- Compute
InfrastructureOffering <|-- Storage
InfrastructureOffering <|-- Connectivity
```



## Service Locations

Any service is produced in availability zones of a provider's infrastructure. Such availability zones are aggregated into data centers which in turn form a provider's covered regions. 

```mermaid
classDiagram

region --> "1..*" dataCenter:aggregationOf

Resource <|-- region
Resource <|-- PhysicalResource 
Resource <|-- availabilityZone

PhysicalResource <|-- dataCenter

dataCenter --> "1..*" availabilityZone:aggregationOf
availabilityZone -->"0..*" Resource:aggregationOf


serviceOffering --> "1..*" availabilityZone:hostedOn
```


For a detailed description of availabilityZone, dataCenter, and region, please see [here](https://gitlab.com/gaia-x/technical-committee/service-characteristics/-/blob/develop/single-point-of-truth/yaml/gax-trust-framework/gax-resource/README.md).

# Storage 

# Compute

Compute Infrastructure services are made out of BareMetal, VirtualMachine, Container, and ComputeFunction services. VirtualMachine and Container also exist in autoscaled versions. 

```mermaid
classDiagram

class Compute {
    <<abstract>>
}


class BareMetal 
class VirtualMachine
class AutoscaledVirtualMachine
class Container
class AutoscaledContainer
class ComputeFunction


Compute <|-- BareMetal 
Compute <|-- VirtualMachine 
VirtualMachine <|-- AutoscaledVirtualMachine
Compute <|-- Container
Container <|-- AutoscaledContainer
Compute <|-- ComputeFunction
```

Each Compute infrastructure service references two important other classes, namely InstatiationRequirements (which are the available configurations of such service), and the available CodeArtifacts, i.e. the SoftwareResources available to be run on such Compute infrastructure service: 

```mermaid
classDiagram

class Compute {
    <<abstract>>
    tenantSeparation
}

class Configuration {
    <<abstract>>
}

class SoftwareResource

class InstantiationRequirements {
    <<abstract>>
}

class CodeArtifact {
    <<abstract>>
}

Compute --> InstantiationRequirements:1..*
Compute --> CodeArtifact:1..*

InstantiationRequirements <|-- Configuration
CodeArtifact <|-- SoftwareResource
```


| Attribute              | Card.  | Comment                                         |
|------------------------|-------|-------------------------------------------------|
| `codeArtifact`        | 1..*  | A structured object of type gax-trust-framework:CodeArtifact | 
| `instantiationReq`    | 1..*  | A structured object of type gax-trust-framework:InstantiationRequirements |
| `tenantSeparation`      | 0..1  | How compute resources of different tenants are separated. Default value = hw-virtualized | 

## Baremetal Compute

```mermaid
classDiagram

class BareMetal 

InstantiationRequirements <|-- ServerFlavor

BareMetal --> ServerFlavor:1..*
BareMetal --> PxeImage:0..*

CodeArtifact <|-- PxeImage
```


For a detailed description of ServerFlavor, see [here](https://gitlab.com/gaia-x/technical-committee/service-characteristics/-/blob/develop/single-point-of-truth/yaml/gax-trust-framework/gax-configuration/README.md).

For a detailed description of PxeImage, see [here](https://gitlab.com/gaia-x/technical-committee/service-characteristics/-/blob/develop/single-point-of-truth/yaml/gax-trust-framework/gax-resource/README.md).

| Attribute              | Card.  | Comment                                         |
|------------------------|-------|-------------------------------------------------|
| `codeArtifact`        | 0..*   | A structured object of type gax-trust-framework:a structured object of type gax-trust-framework:PxeImage | 
| `instantiationReq`    | 1..*   | A structured object of type gax-trust-framework:ServerFlavor |


## Virtual Machine Compute


```mermaid
classDiagram

class VirtualMachine 

InstantiationRequirements <|-- ServerFlavor

VirtualMachine --> ServerFlavor:1..*
VirtualMachine --> VmImage:1..*

CodeArtifact <|-- VmImage
```

For a detailed description of ServerFlavor, see [here](https://gitlab.com/gaia-x/technical-committee/service-characteristics/-/blob/develop/single-point-of-truth/yaml/gax-trust-framework/gax-configuration/README.md).

For a detailed description of VmImage, see [here](https://gitlab.com/gaia-x/technical-committee/service-characteristics/-/blob/develop/single-point-of-truth/yaml/gax-trust-framework/gax-resource/README.md).

| Attribute              | Card. | Comment                                         |
|------------------------|-------|-------------------------------------------------|
| `codeArtifact`        | 1..*   | A structured object of type gax-trust-framework:a structured object of type gax-trust-framework:VmImage | 
| `instantiationReq`    | 1..*   | A structured object of type gax-trust-framework:ServerFlavor |


### Autoscaled VM Compute

For autoscaling, additional configuration in the form of an autoscaling specification: 

```mermaid
classDiagram

class VirtualMachine

class AutoscaledVirtualMachine

VirtualMachine <|-- AutoscaledVirtualMachine
AutoscaledVirtualMachine --> autoscaledVmServiceSpec:1..*

VmAutoscalingGroupSpec <|-- autoscaledVmServiceSpec
InstantiationRequirements <|-- VmAutoscalingGroupSpec
```


For a detailed description of VmAutoscalingGroupSpec, see [here](https://gitlab.com/gaia-x/technical-committee/service-characteristics/-/blob/develop/single-point-of-truth/yaml/gax-trust-framework/gax-configuration/README.md).

| Attribute              | Card.  | Comment                                         |
|------------------------|--------|-------------------------------------------------|
|  `autoscaledVmServiceSpec`| 1..*  | A structured object of type gax-trust-framework:VMAutoScalingGroupSpec | 



## Container Compute

```mermaid
classDiagram

class Container 

InstantiationRequirements <|-- ContainerResourceLimits

Container --> ContainerResourceLimits:1..*
Container --> ContainerImage:1..*

CodeArtifact <|-- ContainerImage
```


For a detailed description of ContainerResourceLimits, see [here](https://gitlab.com/gaia-x/technical-committee/service-characteristics/-/blob/develop/single-point-of-truth/yaml/gax-trust-framework/gax-configuration/README.md).

For a detailed description of ContainerImage, see [here](https://gitlab.com/gaia-x/technical-committee/service-characteristics/-/blob/develop/single-point-of-truth/yaml/gax-trust-framework/gax-resource/README.md).

| Attribute              | Card. | Comment                                         |
|------------------------|-------|-------------------------------------------------|
| `codeArtifact`        | 1..*   | A structured object of type gax-trust-framework:a structured object of type gax-trust-framework:ContainerImage | 
| `instantiationReq`    | 1..*   | A structured object of type gax-trust-framework:ContainerResourceLimits |

_add Autoscaled Container_

## Compute Functions

```mermaid
classDiagram

class ComputeFunction 

InstantiationRequirements <|-- ComputeFunctionTrigger

ComputeFunction --> ComputeFunctionTrigger:1..*
ComputeFunction --> ComputeFunctionCode:1..*

CodeArtifact <|-- ComputeFunctionCode
```


For a detailed description of ComputeFunctionTrigger, see [here](https://gitlab.com/gaia-x/technical-committee/service-characteristics/-/blob/develop/single-point-of-truth/yaml/gax-trust-framework/gax-configuration/README.md).

For a detailed description of ComputeFunctionCode, see [here](https://gitlab.com/gaia-x/technical-committee/service-characteristics/-/blob/develop/single-point-of-truth/yaml/gax-trust-framework/gax-resource/README.md).

| Attribute              | Card.  | Comment                                         |
|------------------------|--------|-------------------------------------------------|
| `codeArtifact`        | 1..*   | A structured object of type gax-trust-framework:a structured object of type gax-trust-framework:ComputeFunctionCode | 
| `instantiationReq`    | 1..*   | A structured object of type gax-trust-framework:ComputeFunctionTrigger |

# Connectivity 

## Physical

## Link 

## Network
