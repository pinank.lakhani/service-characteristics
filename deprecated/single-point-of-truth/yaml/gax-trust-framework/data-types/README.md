# The _data-types_ Folder
This folder contains all classes that describe concepts within the Trust Framework that either do not align to any other folder's convention or that do not warrant a creation of, e.g., a class of the same significance of an Entity of the Gaia-X Conceptual Model (such as _Resource_, _ServiceOffering_, or _Participant_).

Should a datatype gain in importance in the model to finally warrant a description as another _gax-*_ class, the corresponding yaml file must be migrated to the appropriate folder along with appropriate changes to all references to it. 

## Datatypes in Alphabetical Order

Note that if a datatype attribute example value is listed as _one of_ a list of values, it is restricted to only these values. If merely example values are given, no restrictions apply. It is recommended that the author of a Self Description  apply common sense in expanding the value space. While this potentially allows for interim incompatible values it acts as a way of keeping attribute values relevant in quickly developing fields without having to first seek industry alignment.  

### Address

### Agent

### Certificates

### Checksum

### CPU

The CPU datatype describes elements of a physical processor that are important for binary compatibility within e.g. compute services. It is a subclass of _HardwareSpec_.  

| Attribute              | Card. | Comment                                         |
|------------------------|-------|-------------------------------------------------|
| `cpuArchitecture`        | 0..1  |    Basic CPU architecture type. One of 'x86', 'x86-64', 'RISC-V', 'Generic' | 
| `cpuGeneration`    | 0..1  | CPU instruction set generation. Determines basic feature set and migration compatibility. One of 'Skylake-Server-v4', 'Icelake-Server-v4', 'Cascadelake-Server-v4', 'EPYC-Milan-v1', 'EPYC-Rome-v2'. Inspired by https://www.qemu.org/docs/master/system/qemu-cpu-models.html |
| `cpuFlag`      | 0..*  | CPU flags as documented by lscpu and defined in https://github.com/torvalds/linux/blob/master/tools/arch/x86/include/asm/cpufeatures.h . | 
| `smtIsEnabled`        | 0..1  | Is simultaneous multithreading (SMT) or hyper threading (HT) active in this CPU? Default False. | 
| `numberOfSockets`    | 0..1  | Number of CPU Sockets |
| `numberOfCores`      | 0..1  | Number of Cores of the CPU | 
| `numberOfThreads`        | 0..1  | Number of Threads of the CPU | 
| `baseFrequency`    | 0..1  | Frequency of the CPU. A structured object of type measure, e.g. measure:value=3.0 and measure:unit=GHz |
| `boostFrequency`      | 0..1 | Boost frequency of the CPU. A structured object of type measure, e.g. measure:value=3.0 and measure:unit=GHz | 
| `lastLevelCacheSize`        | 0..1 | Last Level Cache size of the CPU. A structure object of type measure, e.g. measure:value=38 and measure:unit=MB | 
| `socket`    | 0..1 | Socket type the CPU fits into. |
| `tdp`      | 0..1 | CPU Thermal Design Power - ref : https://en.wikipedia.org/wiki/Thermal_design_power. A structure object of type measure, e.g. measure:value=100 and measure:unit=W.  | 
| `defaultOverbookingRatio`        | 0..1 | A dimensionless float1 value larger or equal to 1.0 describing the default maximum number of workloads scheduled on this CPU simultaneously. | 
| `supportedOverbookingRatio`    | 0..* | Several dimensionless float values larger or equal to 1.0 describing the available scheduler settings for the numer of simultaneously scheduled workloads on this CPU. |


### DataExport

### Disk

The Disk datatype describes disk-type storage devices with a given capacity and is a subclass of _HardwareSpec_.  

| Attribute              | Card. | Comment                                         |
|------------------------|-------|-------------------------------------------------|
| `size`        | 0..1  |    The storage capacity of that hard drive. A structure objectof type measure, e.g. measure:value=200 and measure:unit=GB. | 
| `type`        | 0..1  |    The type of that drive. One of 'local SSD', 'local HDD', 'shared network storage', 'high-perf NVMe'. | 
| `encryption`        | 0..1  |   Details of disk ecryption. A structured object of type gax-trust-framework:Encryption | 

### Encryption

### Endpoint

### FoafAgent

### FPGA

The FPGA datatype describes an FPGA accelerator card as part of a server platform and is a subclass of _HardwareSpec_.

| Attribute              | Card. | Comment                                         |
|------------------------|-------|-------------------------------------------------|
| `type`        | 0..1  | The FPGA type. | 


### GPU

The GPU datatype describes a GPU accelerator card as part of a server platform and is a subclass of _HardwareSpec_.

| Attribute              | Card. | Comment                                         |
|------------------------|-------|-------------------------------------------------|
| `gpuGeneration`        | 0..1  | GPU generation, as e.g.  'Fermi', 'Kepler', 'Gen9'. | 
| `memory`        | 0..1  | Memory of the GPU. A structure object of type Memory. | 
| `connection`  | 0..1 | I/O interconnection of the GPU, as e.g. 'PCIe Gen4: 64GB/s'. | 


### HardwareSpec

The HardwareSpec datatype holds attributes common to any hardware element used within Self Descriptions.  

| Attribute              | Card. | Comment                                         |
|------------------------|-------|-------------------------------------------------|
| `name`        | 0..1  | Procuct name of the hardware resource, as e.g.  'Intel Xeon Platinum 8260'. | 
| `vendor`        | 0..1  | Vendor of this hardware, as e.g. 'Intel', 'AMD', 'Nvidia'.  | 

### Measure

### Memory

The Memory datatype describes a memory subsystem as part of a server platform, a GPU, a VM, or other forms of computation. It is a subclass of _HardwareSpec_.

| Attribute              | Card. | Comment                                         |
|------------------------|-------|-------------------------------------------------|
| `size`        | 0..1  | Size or capacity of the memory subsystem. A structure object of type measure, e.g. measure:value=24 and measure:unit=GB| 
| `memclass`        | 0..1 | Memory class, e.g. 'DDR4', 'DDR5', 'GDDR5', 'GDDR6'. | 
| `rank`        | 0..1 | DIMM Type wrt rank and buffer architecture, e.g. '1R RDIMM', '2R RDIMM', '4R LRDIMM','8R LRDIMM'| 
| `defaultOverbookingRatio`        | 0..1  | A dimensionless float value larger or equal to 1.0 describing the default overbooking ratio of this memory. | 
| `supportedOverbookingRatio`        | 0..1 | Several dimensionless float values larger or equal to 1.0 describing the available overbooking ratios of this memory. | 


### RegistrationNumber

### ServiceAccessPoint

### Signature

### Standard

### TermsAndConditions
