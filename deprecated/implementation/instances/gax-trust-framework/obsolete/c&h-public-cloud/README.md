# Public Cloud @ Cloud&Heat Technologies

Sample files to model Cloud&Heat Technologies' public cloud offering with Gaia-X Self-Descriptions. There are the following files:

* Self-Description of Cloud&Heat Technologies as `Legal Person`
* Self-Descriptions of Cloud&Heat Technologies' data center "eurotheum" (hosting the offered service) as `Physical Resource`
* Self-Descriptions of two `VM Images` "CentOS 8" and "Fedora 33", from which customers can choose from at instantiation time  
* Self-Descriptions of two `Server flavors` "XS" and "M", from which customers can choose from at instantiation time  
* Self-Description of Cloud&Heat Technologies' public cloud service offering as `Virtual Machine Service Offering`

## Legal Person: Cloud&Heat Technologies

```mermaid
classDiagram

class LegalPerson{
    credentialSubject="did:cloudandheat.com"
    registrationNumber="DEU1104.HRB30549"
    headquartersAddress.countryCode="DE"
    legalAddress.countryCode="DE"
}

```


## Physical Resource: Eurotheum 


```mermaid
classDiagram


class PhysicalResource{
credentialSubject="did:cloudandheat.com/datacenters/eurotheum"
name="Eurotheum"
description="C&H public cloud in Frankfurt am Main."
locationAddress.countryCode=["DE"]
maintainedBy="did:cloudandheat.com"
ownedBy="did:cloudandheat.com"
}


```

Note: Maintaining and owning Participant are referenced by DID.

## VM Image: CentOs 8


```mermaid
classDiagram

class VmImage{
    credentialSubject="did:cloudandheat.com/vm-images/centos-8"
    name "CentOS 8"
    format = "Bare"
    diskFormat = "RAW"
    version = "8"
    license = "https://www.gnu.org/licenses/gpl-3.0.html"
    ramSize = "512 MB"
    filesize = "10 GB"
}

class CheckSum{
    checksum = "5d68f20237c7c01c067b577ee5e490d1"
    checksumAlgorithm = "sha512"
}

VmImage --> "0..1" CheckSum: checkSum

class CPU{
    cpuArchitecture = x86_64""
}

VmImage --> "1..1" CPU: cpuReq

```


## VM Image: Fedora 33

```mermaid
classDiagram

class VmImage{
    credentialSubject="did:web:cloudandheat.com/vm-images/fedora-33"
    name "CentOS 8"
    format = "Bare"
    diskFormat = "RAW"
    version = "33"
    license = "https://www.gnu.org/licenses/gpl-3.0.html"
    ramSize = "512 MB"
    filesize = "4 GB"
}

class CheckSum{
    checksum = "679c8ca014cb3ad1a20410a4f908d23f"
    checksumAlgorithm = "sha512"
}

VmImage --> "0..1" CheckSum: checkSum

class CPU{
    cpuArchitecture = x86_64""
}

VmImage --> "1..1" CPU: cpuReq

```

## Server Flavor: XS

```mermaid
classDiagram


class ServerFlavor{
credentialSubject = "did:web:cloudandheat.com/flavors/xs"
name = "XS"

}

class CPU{
cpuArchitecture  = "x86-64" 
numberOfCores = "1"
}

ServerFlavor --> "1..1" CPU: cpuReq

class Memory{
    size = "1 GB"
}

ServerFlavor --> "1..1" Memory: memoryReq


class Disk{
    size = "10 GB"
    type = "HDD"
}

ServerFlavor --> "1..1" Disk: bootVolumeReq

```


## Server Flavor: M

```mermaid
classDiagram

class ServerFlavor{
credentialSubject = "did:web:cloudandheat.com/flavors/m"
name = "M"

}

class CPU{
cpuArchitecture  = "x86-64" 
numberOfCores = "2"
}

ServerFlavor --> "1..1" CPU: cpuReq

class Memory{
    size = "2 GB"
}

ServerFlavor --> "1..1" Memory: memoryReq


class Disk{
    size = "25 GB"
    type = "HDD"
}

ServerFlavor --> "1..1" Disk: bootVolumeReq

```


## Virtual Machine Service Offering: Cloud&Heat Technologies Public Cloud

```mermaid
classDiagram


class VirtualMachine{
    credentialSubject = "did:web:cloudandheat.com/offerings/eurotheum" 
    name = "C&H Public Cloud"
    tenantSeparation = "virtual"
    instantiationReq = ["did:cloudandheat.com/flavors/xs", "did:cloudandheat.com/flavors/m"]
    codeArtifact = ["did:cloudandheat.com/vm-images/centos-8", "did:cloudandheat.com/vm-images/fedora-33"]
    keyword = ["IaaS", "public cloud"]
    providedBy = "did:cloudandheat.com"
}

class DataAccountExport{
    requestType = "supportCenter"
    accessType = "digital"
    formatType = "application/pdf"
}

class TermsAndConditions{
    content = "https://www.cloudandheat.com/terms-conditions/"
    hash = "3b8a1387b5a80eaa733fc569d64d7e45248bda9a540c0a9ec0aff96e04bbfa02"
}

VirtualMachine --> TermsAndConditions : termsAndConditions
VirtualMachine --> DataAccountExport : dataAccountExport 

```

Note: Providing Participant, Flavors and Images are referenced by DID.




  
