# Service localization - wip

This page illustrates a proposition of concepts to manage the localization of services offered inside a Gaia-X ecosystem. 
It needs to be reviewed and validated by the Gaia-X technical commitee.

```mermaid
classDiagram

Provider  <|-- Participant: Inheritance
Provider "1" -- "*" Location
Provider "1" -- "*" ServiceOffering
class Provider{
  List~Location hasLocations
  List~ServiceOffering hasServiceOffering
}

Location "*" -- "*" ServiceOffering
Location "*" -- "*" LocatedServiceOffering
class Location{
  Provider hasProvider
  List~ServiceOffering canHostServiceOffering
  xsd:string hasAdministrativeLocation
  List~LocatedServiceOffering hasLocatedServiceOffering
}

ServiceOffering "*" -- "*" Location
class ServiceOffering{
  Provider providedBy
  List~Location isAvailableOn
}

LocatedServiceOffering -- ServiceOffering
LocatedServiceOffering "1" -- "*" ComplianceCertificateClaim
LocatedServiceOffering "1" -- "*" ComplianceCertificateCredential
class LocatedServiceOffering{
  ServiceOffering isImplementationOf
  List~Location isHostedOn
  ComplianceCertificateClaim hasComplianceCertificateClaim
  ComplianceCertificateCredential hasComplianceCertificateCredential
}
```

## Remarks

A Provider is a sub class of Participant with additional attributes:
* the list of Locations where this Provider is present
* the unordered list of ServiceOfferings

A ServiceOffering may be available inside several locations (also named "region" or "availability zone" by most of cloud providers). Yet, the consumption of this service generally needs a consumer to indicate the location where the resources must be created. This is why we introduce the concept of LocatedServiceOfferings.

Besides, Compliance for a service is strongly related to location (country, scope of laws) of the service. Thus, ComplianceCertificates apply to LocatedServiceOfferings.