# Toolchain

This folder implements tool chain for Gaia-X Self-Description Schema and Self-Description instances  

Toolchain tasks: 

- Generate JSON-LD context for Gaia-X Credentials
- Generate [Shacl](https://www.w3.org/TR/shacl/) shapes to validate Gaia-X Credentials
- Generate Ontology for Gaia-X Credentials
- Test Shacl shapes by running [unit test](test-shacl). Merging of invalid schemas is prohibited. 
- Validate all Gaia-X Credentials in [instances](../instances) using [validate-instances.py](validate-instances.py). It is not allowed to commit invalid SD instances, anymore

### Validate Gaia-X Self-Description instances

run script from CLI

```shell
python3 validate_instances.py <folder with json-ld files> <folder with shacl shapes>
```


