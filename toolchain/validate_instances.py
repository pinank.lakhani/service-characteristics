from pyshacl import validate
from typing import List

from pyshacl import validate
from rdflib import Graph

import os
import sys

'''
Script to validate all Self-Description instances against a set of shacl shapes. 

Call script with two parameters. 
- First parameter: directory with SD instances as json-ld files. All json files will be considered.
- Second parameter: directory with shacl shapes. All shacl shapes will be considered   

'''


def get_all_files(dir: str, file_extension: str) -> List[str]:
    files = []

    for file in os.listdir(os.path.abspath(dir)):
        path = os.path.join(dir, file)
        if os.path.isdir(path):
            files.extend(get_all_files(path, file_extension))
        else:
            if path.endswith(file_extension):
                files.append(path)
    return files


if __name__ == '__main__':
    shape = os.path.abspath(sys.argv[2])
    if not os.path.exists(shape):
        raise FileNotFoundError("Shape file " + shape + " does not exist.")

    instance_folder = os.path.abspath(sys.argv[1])
    if not os.path.exists(instance_folder):
        raise FileNotFoundError("Instance folder " + instance_folder + " does not exist.")

    print("####################################")
    print("# Validating instances...")
    print("####################################")

    instances = get_all_files(instance_folder, ".json")
    success = True
    for ins_file in instances:
        print(" - Validating " + ins_file + "... ", end="")

        data_graph = Graph().parse(source=ins_file, format="json-ld")
        shape_graph = Graph().parse(source=shape)

        conforms, _, results_text = validate(data_graph,
                                             shape_graph,
                                             inference='rdfs',
                                             advanced=True,
                                             js=True,
                                             meta_shacl=True)
        if conforms:
            print("OK", end="\n")
        else:
            success = False
            print("ERROR", end="\n")
            print(results_text)

    if success:
        sys.exit(0)
    else:
        sys.exit(1)