# Unit test

This folder implements unit testing for the Gaia-X [SHACL](https://www.w3.org/TR/shacl/) files.

The idea is to primarily focus on negative testing to later prevent validation of incorrect credentials.

Test scripts are stored in [src](src) folder. Test Self-Descriptions as JSON-LD files can be found in [data](data) 

## Prerequisites

Please make sure you have the following elements installed on your testing environment :
- NVM
- Docker 

## How to run it

### With NPM

```shell
nvm use || nvm install
npm install
npm run test:local
```

At first, this will install or set the correct version on NodeJS and install the project's dependencies.
Then a LinkML Docker container will be run to generate the necessary Shacl shapes and run the test suite with Vitest. 

### With gitlab-ci-local

Install [gitlab-ci-local](https://github.com/firecow/gitlab-ci-local) and run

```shell
gitlab-ci-local --shell-isolation --needs test-shacl
```

> ⚠️ This method does not work at the moment because of the LinkML Docker container image not being correctly handled by
> Gitlab CI Local.

## Development

Using the [vitest](https://vitest.dev/) framework, the libraries and functions used here are the same than in the [Gaia-X Compliance](https://gitlab.com/gaia-x/lab/compliance/gx-compliance) code.

## Documentations

New to RDF and SHACL? Here are some human readable documentations.  

- Training support: <http://blog.sparna.fr/2022/04/19/supports-de-formation-sparql-cc-by-sa/> (a shorter version in [FR](https://rbdd.cnrs.fr/local/cache-zotspip/23D7N9G8/Francart%20SHApe%20Constraints%20Language.pdf))

- Lecture with examples: <https://www.utwente.nl/en/ces/sal/exams/digital-exams/Linked-Data-and-Semantic-Web/ldsw-lecture7.pdf>

- Book with examples: <https://book.validatingrdf.com/bookHtml011.html>
