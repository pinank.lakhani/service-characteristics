import {defineConfig} from "vitest/config";

export default defineConfig({
    test: {
        logHeapUsage: false,
        poolOptions: {
            threads: {
                minThreads: 1,
                maxThreads: 4
            }
        }
    }
})