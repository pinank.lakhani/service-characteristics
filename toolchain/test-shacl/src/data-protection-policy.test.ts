import {beforeAll, describe, expect, test} from "vitest";
import {ShaclService} from "./ShaclService";
import DatasetExt from 'rdf-ext/lib/Dataset';

import {
    checkNumberOfViolations,
    CLASS_CONSTRAINT,
    DATA_TYPE_CONSTRAINT,
    ERR_MESSAGE_NOT_OF_TYPE_BOOLEAN,
    ERR_MESSAGE_NOT_OF_TYPE_INTEGER,
    EX_PREFIX,
    GX_PREFIX,
    IN_CONSTRAINT,
    test_data_type,
    test_max_vio,
    test_min_vio,
    testValidInstance
} from "../common/utils";

let shaclService: ShaclService;
shaclService = new ShaclService();

let shape: DatasetExt;

beforeAll(async () => {
    shape = await shaclService.loadFromTurtleFile("../../shapes.shacl.ttl");
});
describe("Data Protection Policy Shapes", () => {
    test('Check loaded shape', async () => {
        expect(shape).not.toBeNull();
        expect(shape).toBeInstanceOf(DatasetExt);
    });

    test("Valid instances", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/data-protection-policy-valid.json');
        await testValidInstance(data, shape, shaclService)
    });
});

describe("Data Protection Policy Shapes with wrong cardinalities", () => {
    test("Wrong cardinalities in instance", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/data-protection-policy-wrong-card.json');
	    expect(shape).not.toBeNull();
	    expect(shape).toBeInstanceOf(DatasetExt);	
        const report = await shaclService.validate(shape, data)
//        test_number_of_violations(report, 12)  	
        checkNumberOfViolations(report, 12)
	// Expect 4 errors on backupPolicy
        test_max_vio(report, EX_PREFIX + 'myBackupPolicy', GX_PREFIX + 'protectionFrequency')
        test_min_vio(report, EX_PREFIX + 'myBackupPolicy', GX_PREFIX + 'protectionRetention')
        test_max_vio(report, EX_PREFIX + 'myBackupPolicy', GX_PREFIX + 'protectionMethod')
        test_min_vio(report, EX_PREFIX + 'myBackupPolicy', GX_PREFIX + 'backupLocation')
    // Expect 3 errors on snapshotPolicy
        test_max_vio(report, EX_PREFIX + 'mySnapshotPolicy', GX_PREFIX + 'protectionFrequency')
        test_min_vio(report, EX_PREFIX + 'mySnapshotPolicy', GX_PREFIX + 'protectionRetention')
        test_max_vio(report, EX_PREFIX + 'mySnapshotPolicy', GX_PREFIX + 'protectionMethod')
    // Expect 5 errors on replicationPolicy
        test_max_vio(report, EX_PREFIX + 'myReplicationPolicy', GX_PREFIX + 'protectionFrequency')
        test_min_vio(report, EX_PREFIX + 'myReplicationPolicy', GX_PREFIX + 'protectionRetention')
        test_max_vio(report, EX_PREFIX + 'myReplicationPolicy', GX_PREFIX + 'protectionMethod')
        test_max_vio(report, EX_PREFIX + 'myReplicationPolicy', GX_PREFIX + 'synchronousReplication')
        test_max_vio(report, EX_PREFIX + 'myReplicationPolicy', GX_PREFIX + 'consistencyType')
    });
});

describe("Data Protection Policy Shapes with wrong values", () => {
    test("Wrong values in instances", async () => {
	    let data = await shaclService.loadFromJsonLDFile('data/data-protection-policy-wrong-values.json');
	    expect(shape).not.toBeNull();
	    expect(shape).toBeInstanceOf(DatasetExt);	
        const report = await shaclService.validate(shape, data)
//        test_number_of_violations(report, 16)
        checkNumberOfViolations(report, 22)
        // expect 5 errors in backupPolicy
        test_data_type(report, EX_PREFIX + 'myBackupPolicy', GX_PREFIX + 'protectionFrequency', IN_CONSTRAINT, '') 
        test_data_type(report, EX_PREFIX + 'myBackupPolicy', GX_PREFIX + 'protectionRetention', CLASS_CONSTRAINT, '') 
        test_data_type(report, EX_PREFIX + 'myBackupPolicy', GX_PREFIX + 'protectionMethod', IN_CONSTRAINT, '') 
        test_data_type(report, EX_PREFIX + 'myBackupPolicy', GX_PREFIX + 'backupLocation', CLASS_CONSTRAINT, '') 
        test_data_type(report, EX_PREFIX + 'myBackupPolicy', GX_PREFIX + 'backupReplication', CLASS_CONSTRAINT, '') 
        // expect 4 errors in snapShotPolicy
        test_data_type(report, EX_PREFIX + 'mySnapshotPolicy', GX_PREFIX + 'protectionFrequency', IN_CONSTRAINT, '') 
        test_data_type(report, EX_PREFIX + 'mySnapshotPolicy', GX_PREFIX + 'protectionRetention', CLASS_CONSTRAINT, '') 
        test_data_type(report, EX_PREFIX + 'mySnapshotPolicy', GX_PREFIX + 'protectionMethod', IN_CONSTRAINT, '') 
        test_data_type(report, EX_PREFIX + 'mySnapshotPolicy', GX_PREFIX + 'snapshotReplication', CLASS_CONSTRAINT, '') 
        // expect 7 errors in replicationPolicy
        test_data_type(report, EX_PREFIX + 'myReplicationPolicy', GX_PREFIX + 'protectionFrequency', IN_CONSTRAINT, '') 
        test_data_type(report, EX_PREFIX + 'myReplicationPolicy', GX_PREFIX + 'protectionRetention', CLASS_CONSTRAINT, '') 
        test_data_type(report, EX_PREFIX + 'myReplicationPolicy', GX_PREFIX + 'protectionMethod', IN_CONSTRAINT, '') 
        test_data_type(report, EX_PREFIX + 'myReplicationPolicy', GX_PREFIX + 'synchronousReplication', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_BOOLEAN) 
        test_data_type(report, EX_PREFIX + 'myReplicationPolicy', GX_PREFIX + 'consistencyType', IN_CONSTRAINT, '') 
        test_data_type(report, EX_PREFIX + 'myReplicationPolicy', GX_PREFIX + 'replicaNumber', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_INTEGER)
        test_data_type(report, EX_PREFIX + 'myReplicationPolicy', GX_PREFIX + 'geoReplication', IN_CONSTRAINT, '') 
    });
});

