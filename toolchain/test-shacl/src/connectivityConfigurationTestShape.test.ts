import {beforeAll, describe, expect, test} from "vitest";
import {ShaclService} from "./ShaclService";
import DatasetExt from 'rdf-ext/lib/Dataset';

import {
    testValidInstance,
    EX_PREFIX,
    GX_PREFIX,
    checkNumberOfViolations,
    test_min_vio,    
    test_max_vio,
    test_data_type,
    CLASS_CONSTRAINT,
    DATA_TYPE_CONSTRAINT,
    ERR_MESSAGE_NOT_OF_TYPE_STRING,     
    ERR_MESSAGE_NOT_OF_TYPE_INTEGER,   
    IN_CONSTRAINT,
    DATA_PATTERN_CONSTRAINT,
    MIN_INCLUSIVE_CONSTRAINT,
    MAX_INCLUSIVE_CONSTRAINT,        
    OR_CONSTRAINT
} from "../common/utils";

let shaclService: ShaclService;
shaclService = new ShaclService();

let shape: DatasetExt;

beforeAll(async () => {
    shape = await shaclService.loadFromTurtleFile("../../shapes.shacl.ttl");
});

describe("Connectivity Configuration Shape", () => {
     test('Check loaded shape', async () => {
        expect(shape).not.toBeNull();
        expect(shape).toBeInstanceOf(DatasetExt);
    });
    
    test("Wrong attributes cardinality", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/connectivity-configuration-wrong-card.json');

        const report = await shaclService.validate(shape, data)
        checkNumberOfViolations(report, 2)

        test_max_vio(report, EX_PREFIX + 'myConnectivityConfigurationMaxVio', GX_PREFIX + 'sourceIdentifierA')
        test_max_vio(report, EX_PREFIX + 'myConnectivityConfigurationMaxVio', GX_PREFIX + 'destinationIdentifierZ')
    });
    
    test("Wrong attributes values", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/connectivity-configuration-wrong-values.json');
        
        const report = await shaclService.validate(shape, data)
        checkNumberOfViolations(report, 2)
        
        test_data_type(report, EX_PREFIX + 'myConnectivityConfigurationWrongValues', GX_PREFIX + 'sourceIdentifierA', OR_CONSTRAINT, "")
        test_data_type(report, EX_PREFIX + 'myConnectivityConfigurationWrongValues', GX_PREFIX + 'destinationIdentifierZ', OR_CONSTRAINT, "")
    });

    test("Valid instance", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/connectivity-configuration-valid.json');
        await testValidInstance(data, shape, shaclService)
    });
});

describe("Interconnection Point Identifier Shape", () => {
    test('Check loaded shape', async () => {
       expect(shape).not.toBeNull();
       expect(shape).toBeInstanceOf(DatasetExt);
   });

   test("Wrong attributes cardinality", async () => {
       let data = await shaclService.loadFromJsonLDFile('data/interconnection-point-identifier-wrong-card.json');

       const report = await shaclService.validate(shape, data)
       
       checkNumberOfViolations(report, 7)

       test_min_vio(report, EX_PREFIX + 'myPhysicalInterconnectionPointIdentifierMinVio', GX_PREFIX + 'interconnectionPointIdentifier')
       test_min_vio(report, EX_PREFIX + 'myVirtualInterconnectionPointIdentifierMinVio', GX_PREFIX + 'interconnectionPointIdentifier')

       test_max_vio(report, EX_PREFIX + 'myPhysicalInterconnectionPointIdentifierMaxVio', GX_PREFIX + 'interconnectionPointIdentifier')
       test_max_vio(report, EX_PREFIX + 'myVirtualInterconnectionPointIdentifierMaxVio', GX_PREFIX + 'interconnectionPointIdentifier')

       test_min_vio(report, EX_PREFIX + 'myInterconnectionPointIdentifierMinVio', GX_PREFIX + 'completeIPI')

       test_max_vio(report, EX_PREFIX + 'myInterconnectionPointIdentifierMaxVio', GX_PREFIX + 'macAddress')
       test_max_vio(report, EX_PREFIX + 'myInterconnectionPointIdentifierMaxVio', GX_PREFIX + 'ipAddress')
   });
   
   test("Wrong attributes values", async () => {
       let data = await shaclService.loadFromJsonLDFile('data/interconnection-point-identifier-wrong-values.json');
       
       const report = await shaclService.validate(shape, data)
       
       checkNumberOfViolations(report, 14)
       
       test_data_type(report, EX_PREFIX + 'myPhysicalInterconnectionPointIdentifierWrongVal', GX_PREFIX + 'interconnectionPointIdentifier', CLASS_CONSTRAINT, "")
       test_data_type(report, EX_PREFIX + 'myVirtualInterconnectionPointIdentifierWrongVal', GX_PREFIX + 'interconnectionPointIdentifier', CLASS_CONSTRAINT, "")

       test_data_type(report, EX_PREFIX + 'myInterconnectionPointIdentifierWrongVal', GX_PREFIX + 'ipiType', IN_CONSTRAINT, "")
       test_data_type(report, EX_PREFIX + 'myInterconnectionPointIdentifierWrongVal', GX_PREFIX + 'ipiProvider', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING)
       test_data_type(report, EX_PREFIX + 'myInterconnectionPointIdentifierWrongVal', GX_PREFIX + 'specificParameters', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING)
       test_data_type(report, EX_PREFIX + 'myInterconnectionPointIdentifierWrongVal', GX_PREFIX + 'completeIPI', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING) 
       test_data_type(report, EX_PREFIX + 'myInterconnectionPointIdentifierWrongVal', GX_PREFIX + 'datacenterAllocation', CLASS_CONSTRAINT, "")                               
       test_data_type(report, EX_PREFIX + 'myInterconnectionPointIdentifierWrongVal', GX_PREFIX + 'ipAddress', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING)   
       test_data_type(report, EX_PREFIX + 'myInterconnectionPointIdentifierWrongVal', GX_PREFIX + 'ipAddress', DATA_PATTERN_CONSTRAINT, "")           
       test_data_type(report, EX_PREFIX + 'myInterconnectionPointIdentifierWrongVal', GX_PREFIX + 'macAddress', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING)   
       test_data_type(report, EX_PREFIX + 'myInterconnectionPointIdentifierWrongVal', GX_PREFIX + 'macAddress', DATA_PATTERN_CONSTRAINT, "")           
   });

   test("Valid instance", async () => {
       let data = await shaclService.loadFromJsonLDFile('data/interconnection-point-identifier-valid.json');
       await testValidInstance(data, shape, shaclService)
   });
});

describe("Datacenter Allocation Shape", () => {
    test('Check loaded shape', async () => {
       expect(shape).not.toBeNull();
       expect(shape).toBeInstanceOf(DatasetExt);
   });

   test("Wrong attributes cardinality", async () => {
       let data = await shaclService.loadFromJsonLDFile('data/datacenter-allocation-wrong-card.json');

       const report = await shaclService.validate(shape, data)
       checkNumberOfViolations(report, 6)

       test_min_vio(report, EX_PREFIX + 'myDatacenterAllocationMinVio', GX_PREFIX + 'refersTo')

       test_max_vio(report, EX_PREFIX + 'myDatacenterAllocationMaxVio', GX_PREFIX + 'refersTo')
       test_max_vio(report, EX_PREFIX + 'myDatacenterAllocationMaxVio', GX_PREFIX + 'floor')
       test_max_vio(report, EX_PREFIX + 'myDatacenterAllocationMaxVio', GX_PREFIX + 'rackNumber')
       test_max_vio(report, EX_PREFIX + 'myDatacenterAllocationMaxVio', GX_PREFIX + 'patchPanel')
       test_max_vio(report, EX_PREFIX + 'myDatacenterAllocationMaxVio', GX_PREFIX + 'portNumber')                     
   });
   
   test("Wrong attributes values", async () => {
       let data = await shaclService.loadFromJsonLDFile('data/datacenter-allocation-wrong-values.json');
       
       const report = await shaclService.validate(shape, data)
       checkNumberOfViolations(report, 6)

       test_data_type(report, EX_PREFIX + 'myDatacenterAllocationWrongValues', GX_PREFIX + 'refersTo', CLASS_CONSTRAINT, "")                               
       test_data_type(report, EX_PREFIX + 'myDatacenterAllocationWrongValues', GX_PREFIX + 'floor', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING)   
       test_data_type(report, EX_PREFIX + 'myDatacenterAllocationWrongValues', GX_PREFIX + 'rackNumber', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING)           
       test_data_type(report, EX_PREFIX + 'myDatacenterAllocationWrongValues', GX_PREFIX + 'patchPanel', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING)   
       test_data_type(report, EX_PREFIX + 'myDatacenterAllocationWrongValues', GX_PREFIX + 'portNumber', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_INTEGER)           
   });

   test("Valid instance", async () => {
       let data = await shaclService.loadFromJsonLDFile('data/datacenter-allocation-valid.json');
       await testValidInstance(data, shape, shaclService)
   });
});

describe("VLan configuration test Shape", () => {
    test('Check loaded shape', async () => {
       expect(shape).not.toBeNull();
       expect(shape).toBeInstanceOf(DatasetExt);
   });

   test("Wrong attributes cardinality", async () => {
       let data = await shaclService.loadFromJsonLDFile('data/vlan-configuration-wrong-card.json');

       const report = await shaclService.validate(shape, data)
       checkNumberOfViolations(report, 3)

       test_max_vio(report, EX_PREFIX + 'myvlanConfigurationWringCard', GX_PREFIX + 'vlanType')
       test_max_vio(report, EX_PREFIX + 'myvlanConfigurationWringCard', GX_PREFIX + 'vlanTag')
       test_max_vio(report, EX_PREFIX + 'myvlanConfigurationWringCard', GX_PREFIX + 'vlanEtherType')
   });
   
   test("Wrong attributes values", async () => {
       let data = await shaclService.loadFromJsonLDFile('data/vlan-configuration-wrong-values.json');

       const report = await shaclService.validate(shape, data)
       checkNumberOfViolations(report, 5)
       
       test_data_type(report, EX_PREFIX + 'myvlanConfigurationWrongValues', GX_PREFIX + 'vlanType', IN_CONSTRAINT, "")        
       test_data_type(report, EX_PREFIX + 'myvlanConfigurationWrongValues', GX_PREFIX + 'vlanTag', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_INTEGER)        
       test_data_type(report, EX_PREFIX + 'myvlanConfigurationWrongValues', GX_PREFIX + 'vlanTag', MIN_INCLUSIVE_CONSTRAINT, 'Value is not greater than or equal to 1')
       test_data_type(report, EX_PREFIX + 'myvlanConfigurationWrongValues', GX_PREFIX + 'vlanTag', MAX_INCLUSIVE_CONSTRAINT, 'Value is not less than or equal to 4094')      
       test_data_type(report, EX_PREFIX + 'myvlanConfigurationWrongValues', GX_PREFIX + 'vlanEtherType', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING)          
   });
   
   test("Valid instance", async () => {
       let data = await shaclService.loadFromJsonLDFile('data/vlan-configuration-valid.json');
       await testValidInstance(data, shape, shaclService)
   });
});
