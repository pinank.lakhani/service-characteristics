import {beforeAll, describe, expect, test} from "vitest";
import {ShaclService} from "./ShaclService";
import DatasetExt from 'rdf-ext/lib/Dataset';

import {
    checkNumberOfViolations,
    EX_PREFIX,
    GX_PREFIX,
    IN_CONSTRAINT,
    test_data_type,
    test_max_vio,
    test_min_vio,
    testValidInstance
} from "../common/utils";

let shaclService: ShaclService;
shaclService = new ShaclService();

let shape: DatasetExt;

beforeAll(async () => {
    shape = await shaclService.loadFromTurtleFile("../../shapes.shacl.ttl");
});

describe("Issuer Shape", () => {
    test('Check loaded shape', async () => {
        expect(shape).not.toBeNull();
        expect(shape).toBeInstanceOf(DatasetExt);
    });

    test("Wrong attribute cardinality", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/issuer-wrong-card.json');

        expect(data).not.toBeNull();
        expect(data).toBeInstanceOf(DatasetExt);

        const report = await shaclService.validate(shape, data)
        checkNumberOfViolations(report, 3)

        test_min_vio(report, EX_PREFIX + 'MissingAttr', GX_PREFIX + 'gaiaxTermsAndConditions')
        test_max_vio(report, EX_PREFIX + 'ToMuchAttr', GX_PREFIX + 'gaiaxTermsAndConditions')
        test_data_type(report, EX_PREFIX + 'ToMuchAttr', GX_PREFIX + 'gaiaxTermsAndConditions', IN_CONSTRAINT, '')
    });

    test("Wrong attribute values", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/issuer-wrong-values.json');

        expect(data).not.toBeNull();
        expect(data).toBeInstanceOf(DatasetExt);

        const report = await shaclService.validate(shape, data)
        checkNumberOfViolations(report, 1)

        test_data_type(report, EX_PREFIX + 'Issuer', GX_PREFIX + 'gaiaxTermsAndConditions', IN_CONSTRAINT, '')
    });

    test("Valid instance", async () => {
       let data = await shaclService.loadFromJsonLDFile('data/issuer-valid.json');
        await testValidInstance(data, shape, shaclService)
    });
});

