import {beforeAll, describe, expect, test} from "vitest";
import {ShaclService} from "./ShaclService";
import DatasetExt from 'rdf-ext/lib/Dataset';

import {
    checkNumberOfViolations,
    CLASS_CONSTRAINT,
    DATA_TYPE_CONSTRAINT,
    ERR_MESSAGE_NOT_OF_TYPE_BOOLEAN,
    ERR_MESSAGE_NOT_OF_TYPE_INTEGER,
    EX_PREFIX,
    GX_PREFIX,
    test_data_type,
    test_max_vio,
    test_min_vio,
    testValidInstance
} from "../common/utils";

let shaclService: ShaclService;
shaclService = new ShaclService();

let shape: DatasetExt;

beforeAll(async () => {
    shape = await shaclService.loadFromTurtleFile("../../shapes.shacl.ttl");
});


describe("Container Resource Limits Shape", () => {
    test('Check loaded shape', async () => {
        expect(shape).not.toBeNull();
        expect(shape).toBeInstanceOf(DatasetExt);
    });

    test("Wrong attributes cardinality", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/container-resource-limits-wrong-card.json');

        expect(shape).not.toBeNull();
        expect(shape).toBeInstanceOf(DatasetExt);

        const report = await shaclService.validate(shape, data)
        
        checkNumberOfViolations(report, 8)

        test_max_vio(report, EX_PREFIX + 'myContainerFlavor', GX_PREFIX + 'cpuRequirements')
        test_max_vio(report, EX_PREFIX + 'myContainerFlavor', GX_PREFIX + 'numberOfCoresLimit')
        test_max_vio(report, EX_PREFIX + 'myContainerFlavor', GX_PREFIX + 'memoryRequirements')
        test_max_vio(report, EX_PREFIX + 'myContainerFlavor', GX_PREFIX + 'memoryLimit')
        test_max_vio(report, EX_PREFIX + 'myContainerFlavor', GX_PREFIX + 'gpuRequirements')
        test_max_vio(report, EX_PREFIX + 'myContainerFlavor', GX_PREFIX + 'gpuLimit')
        test_min_vio(report, EX_PREFIX + 'myContainerFlavor', GX_PREFIX + 'confidential')
        test_max_vio(report, EX_PREFIX + 'myContainerFlavor', GX_PREFIX + 'confidentialComputingTechnology')
    });

    test("Wrong attributes values", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/container-resource-limits-wrong-values.json');

        expect(shape).not.toBeNull();
        expect(shape).toBeInstanceOf(DatasetExt);

        const report = await shaclService.validate(shape, data)
        checkNumberOfViolations(report, 13)

        test_data_type(report, EX_PREFIX + 'myContainerFlavor', GX_PREFIX + 'cpuRequirements', CLASS_CONSTRAINT, "")
        test_data_type(report, EX_PREFIX + 'myContainerFlavor', GX_PREFIX + 'numberOfCoresLimit', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_INTEGER)
        test_data_type(report, EX_PREFIX + 'myContainerFlavor', GX_PREFIX + 'memoryRequirements', CLASS_CONSTRAINT, "")
        test_data_type(report, EX_PREFIX + 'myContainerFlavor', GX_PREFIX + 'memoryLimit', CLASS_CONSTRAINT, "")
        test_data_type(report, EX_PREFIX + 'myContainerFlavor', GX_PREFIX + 'gpuRequirements', CLASS_CONSTRAINT, "")
        test_data_type(report, EX_PREFIX + 'myContainerFlavor', GX_PREFIX + 'gpuLimit', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_INTEGER)
        test_data_type(report, EX_PREFIX + 'myContainerFlavor', GX_PREFIX + 'confidential', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_BOOLEAN)
        test_data_type(report, EX_PREFIX + 'myContainerFlavor', GX_PREFIX + 'confidentialComputingTechnology', CLASS_CONSTRAINT, "")
    });

    test("Valid instance", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/container-resource-limits-valid.json');
        await testValidInstance(data, shape, shaclService)
    });
});
