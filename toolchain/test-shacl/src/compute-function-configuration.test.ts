import {beforeAll, describe, expect, test} from "vitest";
import {ShaclService} from "./ShaclService";
import DatasetExt from 'rdf-ext/lib/Dataset';

import {
    testValidInstance,
    EX_PREFIX,
    GX_PREFIX,
//    test_number_of_violations,
    checkNumberOfViolations,
    test_min_vio,
    test_max_vio,
    test_data_type,
    CLASS_CONSTRAINT,
    IN_CONSTRAINT,
    ERR_MESSAGE_NOT_OF_TYPE_INTEGER,
    ERR_MESSAGE_NOT_OF_TYPE_STRING,
    DATA_TYPE_CONSTRAINT
} from "../common/utils";

let shaclService: ShaclService;
shaclService = new ShaclService();

let shape: DatasetExt;

beforeAll(async () => {
    //shape = await shaclService.loadFromTurtleFile("../../gaia-x/shacl/gaia-x.shacl.ttl");
    shape = await shaclService.loadFromTurtleFile("../../shapes.shacl.ttl");
});


describe("Compute Function Configuration Shape", () => {
    test('Check loaded shape', async () => {
        expect(shape).not.toBeNull();
        expect(shape).toBeInstanceOf(DatasetExt);
    });

    test("Valid instance", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/compute-function-configuration-valid.json');
        await testValidInstance(data, shape, shaclService)
    });

    test("Wrong attributes cardinality", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/compute-function-configuration-wrong-card.json')

        expect(shape).not.toBeNull();
        expect(shape).toBeInstanceOf(DatasetExt);
        const report = await shaclService.validate(shape, data)
        checkNumberOfViolations(report,13)

        //  Test count of attributes in Configuration Shape
        test_min_vio(report, EX_PREFIX + 'myWrongComputeFunctionConfiguration', GX_PREFIX + 'computeFunctionRuntime')
        test_max_vio(report, EX_PREFIX + 'myWrongComputeFunctionConfiguration', GX_PREFIX + 'confidentialComputingTechnology')
        //  Test count of attributes in Runtime Shape
        test_min_vio(report, EX_PREFIX + 'myWrongRuntime', GX_PREFIX + 'supportedLanguage')
        test_min_vio(report, EX_PREFIX + 'myWrongRuntime', GX_PREFIX + 'supportedVersion')
        //  Test count of attributes in Quota Shape
        test_max_vio(report, EX_PREFIX + 'myWrongFunctionQuotas', GX_PREFIX + 'functionMaximumNumber')
        test_max_vio(report, EX_PREFIX + 'myWrongFunctionQuotas', GX_PREFIX + 'functionStorageLimit')
        test_max_vio(report, EX_PREFIX + 'myWrongFunctionQuotas', GX_PREFIX + 'functionTimeLimit')
        test_max_vio(report, EX_PREFIX + 'myWrongFunctionQuotas', GX_PREFIX + 'functionMemoryLimit')
        test_max_vio(report, EX_PREFIX + 'myWrongFunctionQuotas', GX_PREFIX + 'functionSizeLimit')
        test_max_vio(report, EX_PREFIX + 'myWrongFunctionQuotas', GX_PREFIX + 'functionConcurrencyLimit')
        test_max_vio(report, EX_PREFIX + 'myWrongFunctionQuotas', GX_PREFIX + 'functionRequestSizeLimit')
        test_max_vio(report, EX_PREFIX + 'myWrongFunctionQuotas', GX_PREFIX + 'functionResponseSizeLimit')
        //  Test count of attributes in Trigger Shape
        test_max_vio(report, EX_PREFIX + 'myWrongTrigger', GX_PREFIX + 'triggeringService')
    });

    test("Wrong attributes values", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/compute-function-configuration-wrong-values.json');

        expect(shape).not.toBeNull();
        expect(shape).toBeInstanceOf(DatasetExt);
        const report = await shaclService.validate(shape, data)
        checkNumberOfViolations(report,32)

        // test wrong values in Configuration Shape
        test_data_type(report, EX_PREFIX + 'myWrongComputeFunctionConfiguration', GX_PREFIX + 'computeFunctionQuotas', CLASS_CONSTRAINT, "")
        test_data_type(report, EX_PREFIX + 'myWrongComputeFunctionConfiguration', GX_PREFIX + 'computeFunctionLibrary', CLASS_CONSTRAINT, "")
        test_data_type(report, EX_PREFIX + 'myWrongComputeFunctionConfiguration', GX_PREFIX + 'computeFunctionRuntime', CLASS_CONSTRAINT, "")
        test_data_type(report, EX_PREFIX + 'myWrongComputeFunctionConfiguration', GX_PREFIX + 'computeFunctionSDK', CLASS_CONSTRAINT, "")
        test_data_type(report, EX_PREFIX + 'myWrongComputeFunctionConfiguration', GX_PREFIX + 'computeFunctionTrigger', CLASS_CONSTRAINT, "")
        test_data_type(report, EX_PREFIX + 'myWrongComputeFunctionConfiguration', GX_PREFIX + 'computeFunctionDeploymentMethod', IN_CONSTRAINT, "")
        test_data_type(report, EX_PREFIX + 'myWrongComputeFunctionConfiguration', GX_PREFIX + 'confidentialComputingTechnology', CLASS_CONSTRAINT, "")
        // test wrong values in Quotas Shape                
        test_data_type(report, EX_PREFIX + 'myWrongFunctionQuotas', GX_PREFIX + 'functionMaximumNumber', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_INTEGER)
        test_data_type(report, EX_PREFIX + 'myWrongFunctionQuotas', GX_PREFIX + 'functionStorageLimit', CLASS_CONSTRAINT, "")
        test_data_type(report, EX_PREFIX + 'myWrongFunctionQuotas', GX_PREFIX + 'functionTimeLimit', CLASS_CONSTRAINT, "")
        test_data_type(report, EX_PREFIX + 'myWrongFunctionQuotas', GX_PREFIX + 'functionMemoryLimit', CLASS_CONSTRAINT, "")
        test_data_type(report, EX_PREFIX + 'myWrongFunctionQuotas', GX_PREFIX + 'functionSizeLimit', CLASS_CONSTRAINT, "")
        test_data_type(report, EX_PREFIX + 'myWrongFunctionQuotas', GX_PREFIX + 'functionConcurrencyLimit', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_INTEGER)
        test_data_type(report, EX_PREFIX + 'myWrongFunctionQuotas', GX_PREFIX + 'functionRequestSizeLimit', CLASS_CONSTRAINT, "")
        test_data_type(report, EX_PREFIX + 'myWrongFunctionQuotas', GX_PREFIX + 'functionResponseSizeLimit', CLASS_CONSTRAINT, "")
        // test wrong values in Runtime Shape
        test_data_type(report, EX_PREFIX + 'myWrongRuntime', GX_PREFIX + 'supportedLanguage', IN_CONSTRAINT, "")
        test_data_type(report, EX_PREFIX + 'myWrongRuntime', GX_PREFIX + 'supportedVersion', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING)
        // test wrong values in Trigger Shape
        test_data_type(report, EX_PREFIX + 'myWrongFunctionTrigger', GX_PREFIX + 'triggeringEvent', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING)
        test_data_type(report, EX_PREFIX + 'myWrongFunctionTrigger', GX_PREFIX + 'triggeringService', CLASS_CONSTRAINT, "")
    });    
});
