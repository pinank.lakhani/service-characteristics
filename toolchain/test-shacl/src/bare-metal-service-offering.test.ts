import {beforeAll, describe, expect, test} from "vitest";
import {ShaclService} from "./ShaclService";
import DatasetExt from 'rdf-ext/lib/Dataset';

import {
    checkNumberOfViolations,
    CLASS_CONSTRAINT,
    EX_PREFIX,
    GX_PREFIX,
    test_data_type,
    test_min_vio,
    testValidInstance
} from "../common/utils";

let shaclService: ShaclService;
shaclService = new ShaclService();

let shape: DatasetExt;

beforeAll(async () => {
    shape = await shaclService.loadFromTurtleFile("../../shapes.shacl.ttl");
});


describe("Bare Metal Service Offering Shape", () => {
    test('Check loaded shape', async () => {
        expect(shape).not.toBeNull();
        expect(shape).toBeInstanceOf(DatasetExt);
    });

    test("Valid instance", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/bare-metal-service-offering-valid.json');
        await testValidInstance(data, shape, shaclService)
    });

    test("Wrong attributes cardinality", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/bare-metal-service-offering-wrong-card.json')

        expect(shape).not.toBeNull();
        expect(shape).toBeInstanceOf(DatasetExt);

        const report = await shaclService.validate(shape, data)
//        test_number_of_violations(report, 2)
        checkNumberOfViolations(report,2)
        //  Test min count of attributes
        test_min_vio(report, EX_PREFIX + 'myBareMetalServiceOffering', GX_PREFIX + 'instantiationReq')
        test_min_vio(report, EX_PREFIX + 'myBareMetalServiceOffering', GX_PREFIX + 'codeArtifact')
    });

    test("Wrong attributes values", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/bare-metal-service-offering-wrong-values.json');

        expect(shape).not.toBeNull();
        expect(shape).toBeInstanceOf(DatasetExt);

        const report = await shaclService.validate(shape, data)
//        test_number_of_violations(report, 2)
        checkNumberOfViolations(report,4)

        test_data_type(report, EX_PREFIX + 'myBareMetalServiceOffering', GX_PREFIX + 'instantiationReq', CLASS_CONSTRAINT, "")
        test_data_type(report, EX_PREFIX + 'myBareMetalServiceOffering', GX_PREFIX + 'codeArtifact', CLASS_CONSTRAINT, "")
    });
    
});
