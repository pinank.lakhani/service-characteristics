import {beforeAll, describe, expect, test} from "vitest";
import {ShaclService} from "./ShaclService";
import DatasetExt from 'rdf-ext/lib/Dataset';

import {
    checkNumberOfViolations,
    EX_PREFIX,
    GX_PREFIX,
    IN_CONSTRAINT,
    test_data_type,
    test_max_vio,
    test_min_vio,
    testValidInstance
} from "../common/utils";

let shaclService: ShaclService;
shaclService = new ShaclService();

let shape: DatasetExt;

beforeAll(async () => {
    shape = await shaclService.loadFromTurtleFile("../../shapes.shacl.ttl");
    expect(shape).not.toBeNull();
    expect(shape).toBeInstanceOf(DatasetExt);
});

describe("Hypervisor Shape", () => {
     test('Check loaded shape', async () => {
        expect(shape).not.toBeNull();
        expect(shape).toBeInstanceOf(DatasetExt);
    });

    test("Wrong attributes cardinality", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/hypervisor-wrong-card.json');

        const report = await shaclService.validate(shape, data)
        
        checkNumberOfViolations(report, 9)

        //  Test max count of attributes
        test_min_vio(report, EX_PREFIX + 'myHypervisorMinVio', GX_PREFIX + 'hypervisorType')
        test_max_vio(report, EX_PREFIX + 'myHypervisorMaxVio', GX_PREFIX + 'hypervisorType')
    });

    test("Wrong attributes values", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/hypervisor-wrong-values.json');
        const report = await shaclService.validate(shape, data)
        
        checkNumberOfViolations(report, 4)
        
         //  Wrong data type
        test_data_type(report, EX_PREFIX + 'myHypervisor', GX_PREFIX + 'hypervisorType', IN_CONSTRAINT, "")
    });

    test("Valid instance", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/hypervisor-valid.json');
        await testValidInstance(data, shape, shaclService)
    });
});
