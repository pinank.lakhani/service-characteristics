import {beforeAll, describe, expect, test} from "vitest";
import {ShaclService} from "./ShaclService";
import DatasetExt from 'rdf-ext/lib/Dataset';

import {
    testValidInstance,
    EX_PREFIX,
    GX_PREFIX,
    checkNumberOfViolations,
    test_min_vio,
    test_max_vio,
    test_data_type,
    IN_CONSTRAINT,        
    CLASS_CONSTRAINT        
} from "../common/utils";

let shaclService: ShaclService;
shaclService = new ShaclService();

let shape: DatasetExt;

beforeAll(async () => {
    shape = await shaclService.loadFromTurtleFile("../../shapes.shacl.ttl");
});

describe("Link Connectivity Service Offering Shape", () => {
     test('Check loaded shape', async () => {
        expect(shape).not.toBeNull();
        expect(shape).toBeInstanceOf(DatasetExt);
    });

    test("Wrong attributes cardinality", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/link-connectivity-service-offering-wrong-card.json');

        const report = await shaclService.validate(shape, data)
        checkNumberOfViolations(report, 3)

        test_min_vio(report, EX_PREFIX + 'myLinkConnectivityConfigurationMinVio', GX_PREFIX + 'connectivityConfiguration')
        test_min_vio(report, EX_PREFIX + 'myLinkConnectivityConfigurationMinVio', GX_PREFIX + 'protocolType')

        test_max_vio(report, EX_PREFIX + 'myLinkConnectivityConfigurationMaxVio', GX_PREFIX + 'protocolType')
    });
    
    test("Wrong attributes values", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/link-connectivity-service-offering-wrong-values.json');

        const report = await shaclService.validate(shape, data)
        checkNumberOfViolations(report, 7)

        test_data_type(report, EX_PREFIX + 'myLinkConnectivityServiceOfferingWrongValues', GX_PREFIX + 'connectivityConfiguration', CLASS_CONSTRAINT, "") 
        test_data_type(report, EX_PREFIX + 'myLinkConnectivityServiceOfferingWrongValues', GX_PREFIX + 'connectivityQoS', CLASS_CONSTRAINT, "") 

        test_data_type(report, EX_PREFIX + 'myLinkConnectivityServiceOfferingWrongValues', GX_PREFIX + 'protocolType', IN_CONSTRAINT, "")        
        test_data_type(report, EX_PREFIX + 'myLinkConnectivityServiceOfferingWrongValues', GX_PREFIX + 'vlanConfiguration', CLASS_CONSTRAINT, "")    
    });
    
    test("Valid instance", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/link-connectivity-service-offering-valid.json');
        await testValidInstance(data, shape, shaclService)
    });
});