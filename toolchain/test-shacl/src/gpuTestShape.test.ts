import {beforeAll, describe, expect, test} from "vitest";
import {ShaclService} from "./ShaclService";
import DatasetExt from 'rdf-ext/lib/Dataset';

import {
    checkNumberOfViolations,
    CLASS_CONSTRAINT,
    DATA_TYPE_CONSTRAINT,
    ERR_MESSAGE_NOT_OF_TYPE_BOOLEAN,
    ERR_MESSAGE_NOT_OF_TYPE_INTEGER,
    EX_PREFIX,
    GX_PREFIX,
    IN_CONSTRAINT,
    MIN_INCLUSIVE_CONSTRAINT,
    test_data_type,
    test_max_vio,
    testValidInstance
} from "../common/utils";

let shaclService: ShaclService;
shaclService = new ShaclService();

let shape: DatasetExt;

beforeAll(async () => {
    shape = await shaclService.loadFromTurtleFile("../../shapes.shacl.ttl");
    expect(shape).not.toBeNull();
    expect(shape).toBeInstanceOf(DatasetExt);
});

describe("GPU Shape", () => {
     test('Check loaded shape', async () => {
        expect(shape).not.toBeNull();
        expect(shape).toBeInstanceOf(DatasetExt);
    });

    test("Wrong attributes cardinality", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/gpu-wrong-card.json');

        const report = await shaclService.validate(shape, data)
        checkNumberOfViolations(report, 4)

        //  Test max count of attributes
        test_max_vio(report, EX_PREFIX + 'myGPUMaxVio', GX_PREFIX + 'gpuInterconnection')
        test_max_vio(report, EX_PREFIX + 'myGPUMaxVio', GX_PREFIX + 'gpuPassthrough')
        test_max_vio(report, EX_PREFIX + 'myGPUMaxVio', GX_PREFIX + 'gpuProcessingUnits')
        test_max_vio(report, EX_PREFIX + 'myGPUMaxVio', GX_PREFIX + 'gpuMemory')
    });

    test("Wrong attributes values", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/gpu-wrong-values.json');
        const report = await shaclService.validate(shape, data)

        checkNumberOfViolations(report, 7)

        //  Wrong data type
        test_data_type(report, EX_PREFIX + 'myGPUDatatypVio', GX_PREFIX + 'gpuInterconnection', IN_CONSTRAINT, "" )
        test_data_type(report, EX_PREFIX + 'myGPUDatatypVio', GX_PREFIX + 'gpuPassthrough', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_BOOLEAN)
        test_data_type(report, EX_PREFIX + 'myGPUDatatypVio', GX_PREFIX + 'gpuProcessingUnits', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_INTEGER)
        test_data_type(report, EX_PREFIX + 'myGPUDatatypVio', GX_PREFIX + 'gpuMemory', CLASS_CONSTRAINT, "")
        
        //  Value not in range
        test_data_type(report, EX_PREFIX + 'myGPUDatatypVio', GX_PREFIX + 'gpuProcessingUnits', MIN_INCLUSIVE_CONSTRAINT, "Value is not greater than or equal to 1")
    });

    test("Valid instance", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/gpu-valid.json');
        await testValidInstance(data, shape, shaclService)
    });
});
