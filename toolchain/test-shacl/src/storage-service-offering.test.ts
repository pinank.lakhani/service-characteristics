import {beforeAll, describe, expect, test} from "vitest";
import {ShaclService} from "./ShaclService";
import DatasetExt from 'rdf-ext/lib/Dataset';

import {
    checkNumberOfViolations,
    CLASS_CONSTRAINT,
    DATA_TYPE_CONSTRAINT,
    ERR_MESSAGE_NOT_OF_TYPE_BOOLEAN,
    ERR_MESSAGE_NOT_OF_TYPE_INTEGER,
    EX_PREFIX,
    GX_PREFIX,
    IN_CONSTRAINT,
    test_data_type,
    test_max_vio,
    test_min_vio,
    testValidInstance
} from "../common/utils";

let shaclService: ShaclService;
shaclService = new ShaclService();

let shape: DatasetExt;

beforeAll(async () => {
    shape = await shaclService.loadFromTurtleFile("../../shapes.shacl.ttl");
});

describe("Storage Service Offering Shapes", () => {
    test('Check loaded shape', async () => {
        expect(shape).not.toBeNull();
        expect(shape).toBeInstanceOf(DatasetExt);
    });
    test("Valid instances", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/storage-service-offering-valid.json');
        await testValidInstance(data, shape, shaclService)
    });
});

describe("Storage Service Offering Shapes with wrong cardinalities", () => {
    test("Wrong cardinalities in instance", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/storage-service-offering-wrong-card.json');
        expect(shape).not.toBeNull();
        expect(shape).toBeInstanceOf(DatasetExt);
        const report = await shaclService.validate(shape, data)
//        test_number_of_violations(report, 13)   
        checkNumberOfViolations(report, 13)                                                                                                                       
        //expect 8 Errors in StorageService                                                                                                                             
        test_max_vio(report, EX_PREFIX + 'myStorageService', GX_PREFIX + 'minimumSize')
        test_max_vio(report, EX_PREFIX + 'myStorageService', GX_PREFIX + 'maximumSize')
        test_min_vio(report, EX_PREFIX + 'myStorageService', GX_PREFIX + 'storageConfiguration')
        test_max_vio(report, EX_PREFIX + 'myStorageService', GX_PREFIX + 'lifetimeManagement')
        test_max_vio(report, EX_PREFIX + 'myStorageService', GX_PREFIX + 'versioning')
        test_max_vio(report, EX_PREFIX + 'myStorageService', GX_PREFIX + 'storageConsistency')
        test_max_vio(report, EX_PREFIX + 'myStorageService', GX_PREFIX + 'dataViews')
        test_max_vio(report, EX_PREFIX + 'myStorageService', GX_PREFIX + 'multipleViews')

        //expect 2 Errors in FileStorageService                                                                                                                         
        test_min_vio(report, EX_PREFIX + 'myFileStorageService', GX_PREFIX + 'storageConfiguration')
        test_max_vio(report, EX_PREFIX + 'myFileStorageService', GX_PREFIX + 'accessSemantics')
        //expect 1 Errors in BlockStorageService                                                                                                                        
        test_min_vio(report, EX_PREFIX + 'myBlockStorageService', GX_PREFIX + 'storageConfiguration')
        //expect 2 Errors in ObjectStorageService                                                                                                                       
        test_min_vio(report, EX_PREFIX + 'myObjectStorageService', GX_PREFIX + 'storageConfiguration')
        test_max_vio(report, EX_PREFIX + 'myObjectStorageService', GX_PREFIX + 'maximumObjectSize')
    });
});

describe("Storage Service Offering Shapes with wrong values", () => {
    test("Wrong values in instance", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/storage-service-offering-wrong-values.json');
        expect(shape).not.toBeNull();
        expect(shape).toBeInstanceOf(DatasetExt);
        const report = await shaclService.validate(shape, data)
//        test_number_of_violations(report, 16)
        checkNumberOfViolations(report,23)
        // Expect 8 Errors in StorageService                                                                                                                            
        test_data_type(report, EX_PREFIX + 'myStorageService', GX_PREFIX + 'minimumSize', CLASS_CONSTRAINT, '')
        test_data_type(report, EX_PREFIX + 'myStorageService', GX_PREFIX + 'maximumSize', CLASS_CONSTRAINT, '')
        test_data_type(report, EX_PREFIX + 'myStorageService', GX_PREFIX + 'storageConfiguration', CLASS_CONSTRAINT, '')
        test_data_type(report, EX_PREFIX + 'myStorageService', GX_PREFIX + 'lifetimeManagement', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_INTEGER)
        test_data_type(report, EX_PREFIX + 'myStorageService', GX_PREFIX + 'versioning', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_BOOLEAN)
        test_data_type(report, EX_PREFIX + 'myStorageService', GX_PREFIX + 'storageConsistency', IN_CONSTRAINT, '')
        test_data_type(report, EX_PREFIX + 'myStorageService', GX_PREFIX + 'dataViews', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_BOOLEAN)
        test_data_type(report, EX_PREFIX + 'myStorageService', GX_PREFIX + 'multipleViews', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_BOOLEAN)

        //expect 3 Errors in FileStorageService                                                                                                                         
        test_data_type(report, EX_PREFIX + 'myFileStorageService', GX_PREFIX + 'storageConfiguration', CLASS_CONSTRAINT, '')
        test_data_type(report, EX_PREFIX + 'myFileStorageService', GX_PREFIX + 'accessSemantics', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_BOOLEAN)
        test_data_type(report, EX_PREFIX + 'myFileStorageService', GX_PREFIX + 'accessAttributes', IN_CONSTRAINT, '')

        //expect 1 Errors in BlockStorageService                                                                                                                        
        test_data_type(report, EX_PREFIX + 'myBlockStorageService', GX_PREFIX + 'storageConfiguration', CLASS_CONSTRAINT, '')

        //expect 4 Errors in ObjectStorageService                                                                                                                       
        test_data_type(report, EX_PREFIX + 'myObjectStorageService', GX_PREFIX + 'storageConfiguration', CLASS_CONSTRAINT, '')
        test_data_type(report, EX_PREFIX + 'myObjectStorageService', GX_PREFIX + 'maximumObjectSize', CLASS_CONSTRAINT, '')
        test_data_type(report, EX_PREFIX + 'myObjectStorageService', GX_PREFIX + 'accessAttributes', IN_CONSTRAINT, '')
        test_data_type(report, EX_PREFIX + 'myObjectStorageService', GX_PREFIX + 'objectAPICompatibility', IN_CONSTRAINT, '')
    });
});

