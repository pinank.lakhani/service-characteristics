import {beforeAll, describe, expect, test} from "vitest";
import {ShaclService} from "./ShaclService";
import DatasetExt from 'rdf-ext/lib/Dataset';

import {
    checkNumberOfViolations,
    EX_PREFIX,
    GX_PREFIX,
    IN_CONSTRAINT,
    test_data_type,
    test_max_vio,
    testValidInstance
} from "../common/utils";

let shaclService: ShaclService;
shaclService = new ShaclService();

let shape: DatasetExt;

beforeAll(async () => {
    shape = await shaclService.loadFromTurtleFile("../../shapes.shacl.ttl");
});


describe("VM Image Shape", () => {
    test('Check loaded shape', async () => {
        expect(shape).not.toBeNull();
        expect(shape).toBeInstanceOf(DatasetExt);
    });

    test("Wrong attributes cardinality", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/vm-image-wrong-card.json');

        expect(shape).not.toBeNull();
        expect(shape).toBeInstanceOf(DatasetExt);

        const report = await shaclService.validate(shape, data)

        checkNumberOfViolations(report, 5)

        //  Test max count of attributes
        test_max_vio(report, EX_PREFIX + 'myVMImageMaxVio', GX_PREFIX + 'watchDogAction')
        test_max_vio(report, EX_PREFIX + 'myVMImageMaxVio', GX_PREFIX + 'firmwareType')
        test_max_vio(report, EX_PREFIX + 'myVMImageMaxVio', GX_PREFIX + 'hwRngTypeOfImage')
        test_max_vio(report, EX_PREFIX + 'myVMImageMaxVio', GX_PREFIX + 'hypervisorType')
        test_max_vio(report, EX_PREFIX + 'myVMImageMaxVio', GX_PREFIX + 'vmImageDiskFormat')
    });

    test("Wrong attributes values", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/vm-image-wrong-values.json');

        expect(shape).not.toBeNull();
        expect(shape).toBeInstanceOf(DatasetExt);

        const report = await shaclService.validate(shape, data)

        checkNumberOfViolations(report, 5)

        test_data_type(report, EX_PREFIX + 'myVMImage', GX_PREFIX + 'watchDogAction', IN_CONSTRAINT, "")
        test_data_type(report, EX_PREFIX + 'myVMImage', GX_PREFIX + 'firmwareType', IN_CONSTRAINT, "")
        test_data_type(report, EX_PREFIX + 'myVMImage', GX_PREFIX + 'hwRngTypeOfImage', IN_CONSTRAINT, "")
        test_data_type(report, EX_PREFIX + 'myVMImage', GX_PREFIX + 'hypervisorType', IN_CONSTRAINT, "")
        test_data_type(report, EX_PREFIX + 'myVMImage', GX_PREFIX + 'vmImageDiskFormat', IN_CONSTRAINT, "")
    });

    test("Valid instance", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/vm-image-valid.json');
        await testValidInstance(data, shape, shaclService)
    });
});
