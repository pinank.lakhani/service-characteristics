import {beforeAll, describe, expect, test} from "vitest";
import {ShaclService} from "./ShaclService";
import DatasetExt from 'rdf-ext/lib/Dataset';

import {
    testValidInstance,
    EX_PREFIX,
    GX_PREFIX,
    checkNumberOfViolations,
    test_min_vio,
    test_max_vio,
    test_data_type,
    CLASS_CONSTRAINT,
    DATA_TYPE_CONSTRAINT, 
    ERR_MESSAGE_NOT_OF_TYPE_BOOLEAN
} from "../common/utils";

let shaclService: ShaclService;
shaclService = new ShaclService();

let shape: DatasetExt;

beforeAll(async () => {
    //shape = await shaclService.loadFromTurtleFile("../../gaia-x/shacl/gaia-x.shacl.ttl");
    shape = await shaclService.loadFromTurtleFile("../../shapes.shacl.ttl");
});


describe("Compute Function Service Offering Shape", () => {
    test('Check loaded shape', async () => {
        expect(shape).not.toBeNull();
        expect(shape).toBeInstanceOf(DatasetExt);
    });

    test("Valid instance", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/compute-function-service-offering-valid.json');
        await testValidInstance(data, shape, shaclService)
    });

    test("Wrong attributes cardinality", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/compute-function-service-offering-wrong-card.json')

        expect(shape).not.toBeNull();
        expect(shape).toBeInstanceOf(DatasetExt);

        const report = await shaclService.validate(shape, data)
        checkNumberOfViolations(report,8)
        //  Test count of attributes for Service Offering Shape
        test_min_vio(report, EX_PREFIX + 'myWrongComputeFunctionServiceOffering', GX_PREFIX + 'computeFunctionConfiguration')        
        test_max_vio(report, EX_PREFIX + 'myWrongComputeFunctionServiceOffering', GX_PREFIX + 'computeFunctionDebugTools')
        test_max_vio(report, EX_PREFIX + 'myWrongComputeFunctionServiceOffering', GX_PREFIX + 'computeFunctionEditor')
        test_max_vio(report, EX_PREFIX + 'myWrongComputeFunctionServiceOffering', GX_PREFIX + 'computeFunctionAllowQuota')
        test_max_vio(report, EX_PREFIX + 'myWrongComputeFunctionServiceOffering', GX_PREFIX + 'computeFunctionAllowTimeout')
        test_max_vio(report, EX_PREFIX + 'myWrongComputeFunctionServiceOffering', GX_PREFIX + 'computeFunctionAllowVersioning')
        test_max_vio(report, EX_PREFIX + 'myWrongComputeFunctionServiceOffering', GX_PREFIX + 'computeFunctionAllowAutoScaling')
        test_max_vio(report, EX_PREFIX + 'myWrongComputeFunctionServiceOffering', GX_PREFIX + 'computeFunctionAllowFSMount')
    });

    test("Wrong attributes values", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/compute-function-service-offering-wrong-values.json');

        expect(shape).not.toBeNull();
        expect(shape).toBeInstanceOf(DatasetExt);

        const report = await shaclService.validate(shape, data)
        checkNumberOfViolations(report,9)

        test_data_type(report, EX_PREFIX + 'myWrongComputeFunctionServiceOffering', GX_PREFIX + 'computeFunctionConfiguration', CLASS_CONSTRAINT, "")
        test_data_type(report, EX_PREFIX + 'myWrongComputeFunctionServiceOffering', GX_PREFIX + 'computeFunctionDebugTools', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_BOOLEAN)
        test_data_type(report, EX_PREFIX + 'myWrongComputeFunctionServiceOffering', GX_PREFIX + 'computeFunctionEditor', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_BOOLEAN)
        test_data_type(report, EX_PREFIX + 'myWrongComputeFunctionServiceOffering', GX_PREFIX + 'computeFunctionAllowQuota', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_BOOLEAN)
        test_data_type(report, EX_PREFIX + 'myWrongComputeFunctionServiceOffering', GX_PREFIX + 'computeFunctionAllowTimeout', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_BOOLEAN)
        test_data_type(report, EX_PREFIX + 'myWrongComputeFunctionServiceOffering', GX_PREFIX + 'computeFunctionAllowVersioning', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_BOOLEAN)
        test_data_type(report, EX_PREFIX + 'myWrongComputeFunctionServiceOffering', GX_PREFIX + 'computeFunctionAllowAutoScaling', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_BOOLEAN)
        test_data_type(report, EX_PREFIX + 'myWrongComputeFunctionServiceOffering', GX_PREFIX + 'computeFunctionAllowFSMount', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_BOOLEAN)
    });    
});
