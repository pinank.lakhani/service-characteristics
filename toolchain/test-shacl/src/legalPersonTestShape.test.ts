import {beforeAll, describe, expect, test} from "vitest";
import {ShaclService} from "./ShaclService";
import DatasetExt from 'rdf-ext/lib/Dataset';

import {
    checkNumberOfViolations,
    CLASS_CONSTRAINT,
    DATA_TYPE_CONSTRAINT,
    ERR_MESSAGE_NOT_OF_TYPE_STRING,
    EX_PREFIX,
    GX_PREFIX,
    OR_CONSTRAINT,
    test_data_type,
    test_max_vio,
    test_min_vio,
    testValidInstance
} from "../common/utils";

let shaclService: ShaclService;
shaclService = new ShaclService();

let shape: DatasetExt;

beforeAll(async () => {
    shape = await shaclService.loadFromTurtleFile("../../shapes.shacl.ttl");
});

describe("Legal Person Shape", () => {
    test('Check loaded shape', async () => {
        expect(shape).not.toBeNull();
        expect(shape).toBeInstanceOf(DatasetExt);
    });

    test("Wrong attribute cardinality", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/legal-person-wrong-card.json');

        expect(data).not.toBeNull();
        expect(data).toBeInstanceOf(DatasetExt);

        const report = await shaclService.validate(shape, data)
        checkNumberOfViolations(report, 5)

        test_min_vio(report, EX_PREFIX + 'minCountVio', GX_PREFIX + 'registrationNumber')
        test_min_vio(report, EX_PREFIX + 'minCountVio', GX_PREFIX + 'headquartersAddress')
        test_min_vio(report, EX_PREFIX + 'minCountVio', GX_PREFIX + 'legalAddress')
        test_max_vio(report, EX_PREFIX + 'maxCountVio', GX_PREFIX + 'headquartersAddress')
        test_max_vio(report, EX_PREFIX + 'maxCountVio', GX_PREFIX + 'legalAddress')
    });

    test("Wrong attribute values", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/legal-person-wrong-values.json');

        expect(data).not.toBeNull();
        expect(data).toBeInstanceOf(DatasetExt);

        const report = await shaclService.validate(shape, data)
        checkNumberOfViolations(report, 9)

        test_data_type(report, EX_PREFIX + 'myLegalParticipant', GX_PREFIX + 'registrationNumber', OR_CONSTRAINT, '')
        test_data_type(report, EX_PREFIX + 'myLegalParticipant', GX_PREFIX + 'headquartersAddress', CLASS_CONSTRAINT, '')
        test_data_type(report, EX_PREFIX + 'myLegalParticipant', GX_PREFIX + 'legalAddress', CLASS_CONSTRAINT, '')
        test_data_type(report, EX_PREFIX + 'myLegalParticipant', GX_PREFIX + 'parentOrganizationOf', CLASS_CONSTRAINT, '')
        test_data_type(report, EX_PREFIX + 'myLegalParticipant', GX_PREFIX + 'subOrganisationOf', CLASS_CONSTRAINT, '')
    });

    test("Valid instance", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/legal-person-valid.json');
        await testValidInstance(data, shape, shaclService)
    });
});

describe("Registration Number Shape", () => {
    test("Wrong attributes cardinality", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/registration-number-wrong-card.json');
        expect(shape).not.toBeNull();
        expect(shape).toBeInstanceOf(DatasetExt);

        const report = await shaclService.validate(shape, data)
        checkNumberOfViolations(report, 10)

        test_min_vio(report, EX_PREFIX + 'localRegistrationNumberMinVio', GX_PREFIX + 'local')
        test_min_vio(report, EX_PREFIX + 'vatIDMinVio', GX_PREFIX + 'vatID')
        test_min_vio(report, EX_PREFIX + 'leiCodeMinVio', 'http://schema.org/leiCode')
        test_min_vio(report, EX_PREFIX + 'eUIDMinVio', GX_PREFIX + 'euid')
        test_min_vio(report, EX_PREFIX + 'eORIMinVio', GX_PREFIX + 'eori')

        test_max_vio(report, EX_PREFIX + 'localRegistrationNumberMaxVio', GX_PREFIX + 'local')
        test_max_vio(report, EX_PREFIX + 'vatIDMaxVio', GX_PREFIX + 'vatID')
        test_max_vio(report, EX_PREFIX + 'leiCodeMaxVio', 'http://schema.org/leiCode')
        test_max_vio(report, EX_PREFIX + 'eUIDMaxVio', GX_PREFIX + 'euid')
        test_max_vio(report, EX_PREFIX + 'eORIMaxVio', GX_PREFIX + 'eori')
    });

    test("Wrong attributes values", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/registration-number-wrong-values.json');
        expect(shape).not.toBeNull();
        expect(shape).toBeInstanceOf(DatasetExt);

        const report = await shaclService.validate(shape, data);
        checkNumberOfViolations(report, 5)

        test_data_type(report, EX_PREFIX + 'localRegistrationNumber', GX_PREFIX + 'local', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING)
        test_data_type(report, EX_PREFIX + 'vatID', GX_PREFIX + 'vatID', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING)
        test_data_type(report, EX_PREFIX + 'leiCode', 'http://schema.org/leiCode', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING)
        test_data_type(report, EX_PREFIX + 'eUID', GX_PREFIX + 'euid', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING)
        test_data_type(report, EX_PREFIX + 'eORI', GX_PREFIX + 'eori', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING)
    });
});



