import {beforeAll, describe, expect, test} from "vitest";
import {ShaclService} from "./ShaclService";
import DatasetExt from 'rdf-ext/lib/Dataset';

import {
    checkNumberOfViolations,
    EX_PREFIX,
    GX_PREFIX,
    OR_CONSTRAINT,
    test_data_type,
    testValidInstance
} from "../common/utils";

let shaclService: ShaclService;
shaclService = new ShaclService();

let shape: DatasetExt;

beforeAll(async () => {
    shape = await shaclService.loadFromTurtleFile("../../shapes.shacl.ttl");
});

describe("AvailabilityZone Shape", () => {
    test('Check loaded shape', () => {
        expect(shape).not.toBeNull();
        expect(shape).toBeInstanceOf(DatasetExt);
    });

    test("Wrong attributes values", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/availability-zone-wrong-values.json');

        const report = await shaclService.validate(shape, data)
        checkNumberOfViolations(report, 1)

        //  availabilityZone.aggregationOf
        test_data_type(report, EX_PREFIX + 'myAvailabilityZoneWrongValues', GX_PREFIX + 'aggregationOfResources', OR_CONSTRAINT, "")
    });

    test("Valid instance", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/availability-zone-valid.json');
        await testValidInstance(data, shape, shaclService)
    });
});

