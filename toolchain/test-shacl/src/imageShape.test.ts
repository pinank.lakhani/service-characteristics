import {beforeAll, describe, expect, test} from "vitest";
import {ShaclService} from "./ShaclService";
import DatasetExt from 'rdf-ext/lib/Dataset';

import {
    checkNumberOfViolations,
    CLASS_CONSTRAINT,
    DATA_TYPE_CONSTRAINT,
    ERR_MESSAGE_NOT_OF_TYPE_BOOLEAN,
    ERR_MESSAGE_NOT_OF_TYPE_DATE,
    ERR_MESSAGE_NOT_OF_TYPE_INTEGER,
    EX_PREFIX,
    GX_PREFIX,
    IN_CONSTRAINT,
    MIN_INCLUSIVE_CONSTRAINT,
    OR_CONSTRAINT,
    test_data_type,
    test_max_vio,
    testValidInstance
} from "../common/utils";

let shaclService: ShaclService;
shaclService = new ShaclService();

let shape: DatasetExt;

beforeAll(async () => {
    shape = await shaclService.loadFromTurtleFile("../../shapes.shacl.ttl");
    expect(shape).not.toBeNull();
    expect(shape).toBeInstanceOf(DatasetExt);
});

describe("Image Shape", () => {
     test('Check loaded shape', async () => {
        expect(shape).not.toBeNull();
        expect(shape).toBeInstanceOf(DatasetExt);
    });

    test("Wrong attributes cardinality", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/image-wrong-card.json');

        const report = await shaclService.validate(shape, data)
        checkNumberOfViolations(report,14)

        //  Test max count of attributes
        test_max_vio(report, EX_PREFIX + 'myImageMaxVio', GX_PREFIX + 'vPMU')
        test_max_vio(report, EX_PREFIX + 'myImageMaxVio', GX_PREFIX + 'cpuReq')
        test_max_vio(report, EX_PREFIX + 'myImageMaxVio', GX_PREFIX + 'maintenance')
        test_max_vio(report, EX_PREFIX + 'myImageMaxVio', GX_PREFIX + 'fileSize')
        test_max_vio(report, EX_PREFIX + 'myImageMaxVio', GX_PREFIX + 'multiQueues')
        test_max_vio(report, EX_PREFIX + 'myImageMaxVio', GX_PREFIX + 'secureBoot')
        test_max_vio(report, EX_PREFIX + 'myImageMaxVio', GX_PREFIX + 'updateStrategy')
        test_max_vio(report, EX_PREFIX + 'myImageMaxVio', GX_PREFIX + 'encryption')
        test_max_vio(report, EX_PREFIX + 'myImageMaxVio', GX_PREFIX + 'licenseIncluded')
        test_max_vio(report, EX_PREFIX + 'myImageMaxVio', GX_PREFIX + 'ramReq')
        test_max_vio(report, EX_PREFIX + 'myImageMaxVio', GX_PREFIX + 'operatingSystem')
        test_max_vio(report, EX_PREFIX + 'myImageMaxVio', GX_PREFIX + 'rootDiskReq')
        test_max_vio(report, EX_PREFIX + 'myImageMaxVio', GX_PREFIX + 'gpuReq')
        test_max_vio(report, EX_PREFIX + 'myImageMaxVio', GX_PREFIX + 'videoRamSize')
    });

    test("Wrong attributes values", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/image-wrong-values.json');
        const report = await shaclService.validate(shape, data)

        checkNumberOfViolations(report, 24)
        
        //  Wrong data type
        test_data_type(report, EX_PREFIX + 'myImage', GX_PREFIX + 'vPMU', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_BOOLEAN)
        test_data_type(report, EX_PREFIX + 'myImage', GX_PREFIX + 'cpuReq', CLASS_CONSTRAINT, "")
        test_data_type(report, EX_PREFIX + 'myImage', GX_PREFIX + 'maintenance', CLASS_CONSTRAINT, "")
        test_data_type(report, EX_PREFIX + 'myImage', GX_PREFIX + 'fileSize', CLASS_CONSTRAINT, "")
        test_data_type(report, EX_PREFIX + 'myImage', GX_PREFIX + 'multiQueues', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_BOOLEAN)
        test_data_type(report, EX_PREFIX + 'myImage', GX_PREFIX + 'secureBoot', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_BOOLEAN)
        test_data_type(report, EX_PREFIX + 'myImage', GX_PREFIX + 'updateStrategy', CLASS_CONSTRAINT, "")
        test_data_type(report, EX_PREFIX + 'myImage', GX_PREFIX + 'encryption', CLASS_CONSTRAINT, "")
        test_data_type(report, EX_PREFIX + 'myImage', GX_PREFIX + 'licenseIncluded', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_BOOLEAN)
        test_data_type(report, EX_PREFIX + 'myImage', GX_PREFIX + 'ramReq', CLASS_CONSTRAINT, "")
        test_data_type(report, EX_PREFIX + 'myImage', GX_PREFIX + 'operatingSystem', CLASS_CONSTRAINT, "")
        test_data_type(report, EX_PREFIX + 'myImage', GX_PREFIX + 'rootDiskReq', CLASS_CONSTRAINT, "")
        test_data_type(report, EX_PREFIX + 'myImage', GX_PREFIX + 'gpuReq', CLASS_CONSTRAINT, "")
        test_data_type(report, EX_PREFIX + 'myImage', GX_PREFIX + 'videoRamSize', CLASS_CONSTRAINT, "")
    });

    test("Valid instance", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/image-valid.json');
        await testValidInstance(data, shape, shaclService)
    });
});

describe("Update Strategy Shape", () => {
    test('Check loaded shape', async () => {
       expect(shape).not.toBeNull();
       expect(shape).toBeInstanceOf(DatasetExt);
   });

   test("Wrong attributes cardinality", async () => {
       let data = await shaclService.loadFromJsonLDFile('data/update-strategy-wrong-card.json');

       const report = await shaclService.validate(shape, data)
       checkNumberOfViolations(report, 4)
       
       //  Test max count of attributes
       test_max_vio(report, EX_PREFIX + 'myUpdateStrategyMaxVio', GX_PREFIX + 'replaceFrequency')
       test_max_vio(report, EX_PREFIX + 'myUpdateStrategyMaxVio', GX_PREFIX + 'hotfixHours')
       test_max_vio(report, EX_PREFIX + 'myUpdateStrategyMaxVio', GX_PREFIX + 'oldVersionsValidUntil')
       test_max_vio(report, EX_PREFIX + 'myUpdateStrategyMaxVio', GX_PREFIX + 'providedUntil')
   });

   test("Wrong attributes values", async () => {
       let data = await shaclService.loadFromJsonLDFile('data/update-strategy-wrong-values.json');
       const report = await shaclService.validate(shape, data)
    
       checkNumberOfViolations(report, 5)
       
       //  Wrong data type
       test_data_type(report, EX_PREFIX + 'myUpdateStrategy', GX_PREFIX + 'replaceFrequency', IN_CONSTRAINT, "")
       test_data_type(report, EX_PREFIX + 'myUpdateStrategy', GX_PREFIX + 'hotfixHours', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_INTEGER)
       test_data_type(report, EX_PREFIX + 'myUpdateStrategy', GX_PREFIX + 'oldVersionsValidUntil', OR_CONSTRAINT, "")
       test_data_type(report, EX_PREFIX + 'myUpdateStrategy', GX_PREFIX + 'providedUntil', OR_CONSTRAINT, "")
   
       //  Value not in range
       test_data_type(report, EX_PREFIX + 'myUpdateStrategy', GX_PREFIX + 'hotfixHours', MIN_INCLUSIVE_CONSTRAINT, "Value is not greater than or equal to 0")
    });

   test("Valid instance", async () => {
       let data = await shaclService.loadFromJsonLDFile('data/update-strategy-valid.json');
       await testValidInstance(data, shape, shaclService)
   });
});

describe("Maintenance Subscription Shape", () => {
    test('Check loaded shape', async () => {
       expect(shape).not.toBeNull();
       expect(shape).toBeInstanceOf(DatasetExt);
   });

   test("Wrong attributes cardinality", async () => {
       let data = await shaclService.loadFromJsonLDFile('data/maintenance-substription-wrong-card.json');

       const report = await shaclService.validate(shape, data)
       checkNumberOfViolations(report, 3)

       //  Test max count of attributes
       test_max_vio(report, EX_PREFIX + 'myMaintenanceSubscriptionMaxVio', GX_PREFIX + 'subscriptionIncluded')
       test_max_vio(report, EX_PREFIX + 'myMaintenanceSubscriptionMaxVio', GX_PREFIX + 'subscriptionRequired')
       test_max_vio(report, EX_PREFIX + 'myMaintenanceSubscriptionMaxVio', GX_PREFIX + 'maintainedUntil')
   });

   test("Wrong attributes values", async () => {
       let data = await shaclService.loadFromJsonLDFile('data/maintenance-substription-wrong-values.json');
       const report = await shaclService.validate(shape, data)

       checkNumberOfViolations(report, 3)
       
       //  Wrong data type
       test_data_type(report, EX_PREFIX + 'myMaintenanceSubscription', GX_PREFIX + 'subscriptionIncluded', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_BOOLEAN)
       test_data_type(report, EX_PREFIX + 'myMaintenanceSubscription', GX_PREFIX + 'subscriptionRequired', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_BOOLEAN)
       test_data_type(report, EX_PREFIX + 'myMaintenanceSubscription', GX_PREFIX + 'maintainedUntil', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_DATE)
   });

   test("Valid instance", async () => {
       let data = await shaclService.loadFromJsonLDFile('data/maintenance-substription-valid.json');
       await testValidInstance(data, shape, shaclService)
   });
});
