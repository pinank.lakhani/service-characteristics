import {beforeAll, describe, expect, test} from "vitest";
import {ShaclService} from "./ShaclService";
import DatasetExt from 'rdf-ext/lib/Dataset';

import {
    checkNumberOfViolations,
    CLASS_CONSTRAINT,
    DATA_TYPE_CONSTRAINT,
    ERR_MESSAGE_NOT_OF_TYPE_BOOLEAN,
    ERR_MESSAGE_NOT_OF_TYPE_STRING,
    EX_PREFIX,
    GX_PREFIX,
    IN_CONSTRAINT,
    test_data_type,
    test_max_vio,
    test_min_vio,
    testValidInstance,
} from "../common/utils";

let shaclService: ShaclService;
shaclService = new ShaclService();

let shape: DatasetExt;

beforeAll(async () => {
    shape = await shaclService.loadFromTurtleFile("../../shapes.shacl.ttl");
});

describe("Server Flavor Shape", () => {
    test('Check loaded shape', async () => {
        expect(shape).not.toBeNull();
        expect(shape).toBeInstanceOf(DatasetExt);
    });

    test("Wrong attributes cardinality", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/server-flavor-wrong-card.json');

        expect(shape).not.toBeNull();
        expect(shape).toBeInstanceOf(DatasetExt);

        const report = await shaclService.validate(shape, data)
        checkNumberOfViolations(report, 12)

        test_min_vio(report, EX_PREFIX + 'myServerFlavorMinVio', GX_PREFIX + 'cpu')
        test_min_vio(report, EX_PREFIX + 'myServerFlavorMinVio', GX_PREFIX + 'ram')
        test_min_vio(report, EX_PREFIX + 'myServerFlavorMinVio', GX_PREFIX + 'bootVolume')

        test_max_vio(report, EX_PREFIX + 'myServerFlavorMaxVio', GX_PREFIX + 'cpu')
        test_max_vio(report, EX_PREFIX + 'myServerFlavorMaxVio', GX_PREFIX + 'ram')
        test_max_vio(report, EX_PREFIX + 'myServerFlavorMaxVio', GX_PREFIX + 'gpu')
        test_max_vio(report, EX_PREFIX + 'myServerFlavorMaxVio', GX_PREFIX + 'network')
        test_max_vio(report, EX_PREFIX + 'myServerFlavorMaxVio', GX_PREFIX + 'bootVolume')
        test_max_vio(report, EX_PREFIX + 'myServerFlavorMaxVio', GX_PREFIX + 'confidentialComputing')
        test_max_vio(report, EX_PREFIX + 'myServerFlavorMaxVio', GX_PREFIX + 'hypervisor')
        test_max_vio(report, EX_PREFIX + 'myServerFlavorMaxVio', GX_PREFIX + 'hardwareAssistedVirtualization')
        test_max_vio(report, EX_PREFIX + 'myServerFlavorMaxVio', GX_PREFIX + 'hwRngTypeOfFlavor')
    });

    test("Wrong attributes values", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/server-flavor-wrong-values.json');

        expect(shape).not.toBeNull();
        expect(shape).toBeInstanceOf(DatasetExt);

        const report = await shaclService.validate(shape, data)
        checkNumberOfViolations(report, 17)

        test_data_type(report, EX_PREFIX + 'myServerFlavorWrongValues', GX_PREFIX + 'cpu', CLASS_CONSTRAINT, '')
        test_data_type(report, EX_PREFIX + 'myServerFlavorWrongValues', GX_PREFIX + 'ram', CLASS_CONSTRAINT, '')
        test_data_type(report, EX_PREFIX + 'myServerFlavorWrongValues', GX_PREFIX + 'bootVolume', CLASS_CONSTRAINT, '')
        test_data_type(report, EX_PREFIX + 'myServerFlavorWrongValues', GX_PREFIX + 'gpu', CLASS_CONSTRAINT, '')
        test_data_type(report, EX_PREFIX + 'myServerFlavorWrongValues', GX_PREFIX + 'network', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING)
        test_data_type(report, EX_PREFIX + 'myServerFlavorWrongValues', GX_PREFIX + 'additionalVolume', CLASS_CONSTRAINT, '')
        test_data_type(report, EX_PREFIX + 'myServerFlavorWrongValues', GX_PREFIX + 'confidentialComputing', CLASS_CONSTRAINT, '')
        test_data_type(report, EX_PREFIX + 'myServerFlavorWrongValues', GX_PREFIX + 'hypervisor', CLASS_CONSTRAINT, '')
        test_data_type(report, EX_PREFIX + 'myServerFlavorWrongValues', GX_PREFIX + 'hardwareAssistedVirtualization', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_BOOLEAN)
        test_data_type(report, EX_PREFIX + 'myServerFlavorWrongValues', GX_PREFIX + 'hwRngTypeOfFlavor', IN_CONSTRAINT, '')
    });

    test("Valid instance", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/server-flavor-valid.json');
        await testValidInstance(data, shape, shaclService)
    });

});
