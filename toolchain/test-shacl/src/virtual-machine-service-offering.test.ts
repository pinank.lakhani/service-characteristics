import {beforeAll, describe, expect, test} from "vitest";
import {ShaclService} from "./ShaclService";
import DatasetExt from 'rdf-ext/lib/Dataset';

import {
    checkNumberOfViolations,
    CLASS_CONSTRAINT,
    EX_PREFIX,
    GX_PREFIX,
    test_data_type,
    test_min_vio,
    testValidInstance
} from "../common/utils";

let shaclService: ShaclService;
shaclService = new ShaclService();

let shape: DatasetExt;

beforeAll(async () => {
    shape = await shaclService.loadFromTurtleFile("../../shapes.shacl.ttl");
});


describe("Virtual Machine Service Offering Shape", () => {
    test('Check loaded shape', async () => {
        expect(shape).not.toBeNull();
        expect(shape).toBeInstanceOf(DatasetExt);
    });

    test("Wrong attributes cardinality", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/virtual-machine-service-offering-wrong-card.json')

        expect(shape).not.toBeNull();
        expect(shape).toBeInstanceOf(DatasetExt);

        const report = await shaclService.validate(shape, data)
        checkNumberOfViolations(report, 2)

        //  Test min count of attributes
        test_min_vio(report, EX_PREFIX + 'myVirtualMachineServiceOfferingMinVio', GX_PREFIX + 'instantiationReq')
        test_min_vio(report, EX_PREFIX + 'myVirtualMachineServiceOfferingMinVio', GX_PREFIX + 'codeArtifact')
    });

    test("Wrong attributes values", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/virtual-machine-service-offering-wrong-values.json');

        expect(shape).not.toBeNull();
        expect(shape).toBeInstanceOf(DatasetExt);

        const report = await shaclService.validate(shape, data)
        checkNumberOfViolations(report, 4)

        test_data_type(report, EX_PREFIX + 'myVirtualMachineServiceOffering', GX_PREFIX + 'instantiationReq', CLASS_CONSTRAINT, "")
        test_data_type(report, EX_PREFIX + 'myVirtualMachineServiceOffering', GX_PREFIX + 'codeArtifact', CLASS_CONSTRAINT, "")
    });

    test("Valid instance", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/virtual-machine-service-offering-valid.json');
        await testValidInstance(data, shape, shaclService)
    });
});
