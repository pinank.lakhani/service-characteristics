import {beforeAll, describe, expect, test} from "vitest";
import {ShaclService} from "./ShaclService";
import DatasetExt from 'rdf-ext/lib/Dataset';

import {
    checkNumberOfViolations,
    DATA_TYPE_CONSTRAINT,
    ERR_MESSAGE_NOT_OF_TYPE_FLOAT,
    ERR_MESSAGE_NOT_OF_TYPE_STRING,
    EX_PREFIX,
    QUDT_PREFIX,
    test_data_type,
    test_max_vio,
    test_min_vio,
    testValidInstance,
} from "../common/utils";

let shaclService: ShaclService;
shaclService = new ShaclService();

let shape: DatasetExt;

beforeAll(async () => {
    shape = await shaclService.loadFromTurtleFile("../../shapes.shacl.ttl");
    expect(shape).not.toBeNull();
    expect(shape).toBeInstanceOf(DatasetExt);
});

describe("Quantity Shape", () => {
     test('Check loaded shape', async () => {
        expect(shape).not.toBeNull();
        expect(shape).toBeInstanceOf(DatasetExt);
    });

    test("Wrong attributes cardinality", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/quantity-wrong-card.json');

        const report = await shaclService.validate(shape, data)
        checkNumberOfViolations(report, 4)

        //  Test min count attributes
        test_min_vio(report, EX_PREFIX + 'myQuantityMinVio', QUDT_PREFIX + 'value')
        test_min_vio(report, EX_PREFIX + 'myQuantityMinVio', QUDT_PREFIX + 'unit')

        //  Test max count of attributes
        test_max_vio(report, EX_PREFIX + 'myQuantityMaxVio', QUDT_PREFIX + 'value')
        test_max_vio(report, EX_PREFIX + 'myQuantityMaxVio', QUDT_PREFIX + 'unit')
    });

    test("Wrong attributes values", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/quantity-wrong-values.json');
        const report = await shaclService.validate(shape, data)

        checkNumberOfViolations(report, 4)
       
        //  Wrong data type
        test_data_type(report, EX_PREFIX + 'myQuantityDataTypeVio', QUDT_PREFIX + 'value', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_FLOAT)
        test_data_type(report, EX_PREFIX + 'myQuantityDataTypeVio', QUDT_PREFIX + 'unit', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING)

        //  Value not in range
        //  TODO: implement test case, as soon as equals_expression is supported by shacl gen 
        //test_data_type(report, EX_PREFIX + 'myQuantityRestrictionVio', QUDT_PREFIX + 'value', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_FLOAT)
    });

    test("Valid instance", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/quantity-valid.json');
        await testValidInstance(data, shape, shaclService)
    });
});
