import {beforeAll, describe, expect, test} from "vitest";
import {ShaclService} from "./ShaclService";
import DatasetExt from 'rdf-ext/lib/Dataset';

import {
    checkNumberOfViolations,
    CLASS_CONSTRAINT,
    DATA_TYPE_CONSTRAINT,
    ERR_MESSAGE_NOT_OF_TYPE_ANY_URI,
    ERR_MESSAGE_NOT_OF_TYPE_STRING,
    EX_PREFIX,
    GX_PREFIX,
    test_data_type,
    test_max_vio,
    test_min_vio,
    testValidInstance
} from "../common/utils";

let shaclService: ShaclService;
shaclService = new ShaclService();

let shape: DatasetExt;

beforeAll(async () => {
    shape = await shaclService.loadFromTurtleFile("../../shapes.shacl.ttl");
});

describe("Endpoint Shape", () => {
    test('Check loaded shape', async () => {
        expect(shape).not.toBeNull();
        expect(shape).toBeInstanceOf(DatasetExt);
    });

    test("Wrong attributes cardinality", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/endpoint-wrong-card.json');

        expect(shape).not.toBeNull();
        expect(shape).toBeInstanceOf(DatasetExt);

        const report = await shaclService.validate(shape, data)
        checkNumberOfViolations(report, 3)

        test_min_vio(report, EX_PREFIX + 'myEndpointMinVio', GX_PREFIX + 'standardConformity')
        test_max_vio(report, EX_PREFIX + 'myEndpointMaxVio', GX_PREFIX + 'formalDescription')
        test_max_vio(report, EX_PREFIX + 'myEndpointMaxVio', GX_PREFIX + 'endpointURL')
    });

    test("Wrong attributes values", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/endpoint-wrong-values.json');

        expect(shape).not.toBeNull();
        expect(shape).toBeInstanceOf(DatasetExt);

        const report = await shaclService.validate(shape, data)
        checkNumberOfViolations(report, 4)

        test_data_type(report, EX_PREFIX + 'myEndpoint', GX_PREFIX + 'endpointURL', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_ANY_URI)
        test_data_type(report, EX_PREFIX + 'myEndpoint', GX_PREFIX + 'formalDescription', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING)
        test_data_type(report, EX_PREFIX + 'myEndpoint', GX_PREFIX + 'standardConformity', CLASS_CONSTRAINT, '')
    });

    test("Valid instance", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/endpoint-valid.json');
        await testValidInstance(data, shape, shaclService)
    });
});

