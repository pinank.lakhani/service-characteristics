import {beforeAll, describe, expect, test} from "vitest";
import {ShaclService} from "./ShaclService";
import DatasetExt from 'rdf-ext/lib/Dataset';

import {
    checkNumberOfViolations,
    CLASS_CONSTRAINT,
    DATA_TYPE_CONSTRAINT,
    ERR_MESSAGE_NOT_OF_TYPE_ANY_URI,
    ERR_MESSAGE_NOT_OF_TYPE_STRING,
    EX_PREFIX,
    GX_PREFIX,
    IN_CONSTRAINT,
    OR_CONSTRAINT,
    test_data_type,
    test_max_vio,
    test_min_vio,
    testValidInstance,
} from "../common/utils";

let shaclService: ShaclService;
shaclService = new ShaclService();

let shape: DatasetExt;

beforeAll(async () => {
    shape = await shaclService.loadFromTurtleFile("../../shapes.shacl.ttl");
});

describe("Service Offering Shape", () => {
    test('Check loaded shape', async () => {
        expect(shape).not.toBeNull();
        expect(shape).toBeInstanceOf(DatasetExt);
    });

    test("Wrong attributes cardinality", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/service-offering-wrong-card.json');

        expect(shape).not.toBeNull();
        expect(shape).toBeInstanceOf(DatasetExt);

        const report = await shaclService.validate(shape, data)
        checkNumberOfViolations(report, 7)

        test_min_vio(report, EX_PREFIX + 'myServiceOfferingMinVio', GX_PREFIX + 'providedBy')
        test_min_vio(report, EX_PREFIX + 'myServiceOfferingMinVio', GX_PREFIX + 'servicePolicy')
        test_min_vio(report, EX_PREFIX + 'myServiceOfferingMinVio', GX_PREFIX + 'serviceOfferingTermsAndConditions')
        test_min_vio(report, EX_PREFIX + 'myServiceOfferingMinVio', GX_PREFIX + 'dataAccountExport')

        test_max_vio(report, EX_PREFIX + 'myServiceOfferingMaxVio', GX_PREFIX + 'provisionType')
        test_max_vio(report, EX_PREFIX + 'myServiceOfferingMaxVio', GX_PREFIX + 'endpoint')
        test_max_vio(report, EX_PREFIX + 'myServiceOfferingMaxVio', GX_PREFIX + 'providedBy')
    });

    test("Wrong attributes values", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/service-offering-wrong-values.json');

        expect(shape).not.toBeNull();
        expect(shape).toBeInstanceOf(DatasetExt);

        const report = await shaclService.validate(shape, data)
        checkNumberOfViolations(report, 15)

        test_data_type(report, EX_PREFIX + 'myServiceOffering', GX_PREFIX + 'providedBy', CLASS_CONSTRAINT, '')
        test_data_type(report, EX_PREFIX + 'myServiceOffering', GX_PREFIX + 'dependsOn', OR_CONSTRAINT, '')
        test_data_type(report, EX_PREFIX + 'myServiceOffering', GX_PREFIX + 'aggregationOfResources', OR_CONSTRAINT, '')
        test_data_type(report, EX_PREFIX + 'myServiceOffering', GX_PREFIX + 'serviceOfferingTermsAndConditions', CLASS_CONSTRAINT, '')
        test_data_type(report, EX_PREFIX + 'myServiceOffering', GX_PREFIX + 'servicePolicy', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING)
        test_data_type(report, EX_PREFIX + 'myServiceOffering', GX_PREFIX + 'dataAccountExport', CLASS_CONSTRAINT, '')
        test_data_type(report, EX_PREFIX + 'myServiceOffering', GX_PREFIX + 'keyword', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING)
        test_data_type(report, EX_PREFIX + 'myServiceOffering', GX_PREFIX + 'provisionType', IN_CONSTRAINT, '')
        test_data_type(report, EX_PREFIX + 'myServiceOffering', GX_PREFIX + 'dataProtectionRegime', IN_CONSTRAINT, '')
        test_data_type(report, EX_PREFIX + 'myServiceOffering', GX_PREFIX + 'endpoint', CLASS_CONSTRAINT, '')
        test_data_type(report, EX_PREFIX + 'myServiceOffering', GX_PREFIX + 'dataAccountExport', CLASS_CONSTRAINT, '')
        test_data_type(report, EX_PREFIX + 'myServiceOffering', GX_PREFIX + 'hostedOn', OR_CONSTRAINT, '')
    });

    test("Valid instance", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/service-offering-valid.json');
        await testValidInstance(data, shape, shaclService)
    });
});

describe("Terms And Conditions Test Shape", () => {
    test("Wrong attributes cardinality", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/terms-and-conditions-wrong-card.json');

        expect(shape).not.toBeNull();
        expect(shape).toBeInstanceOf(DatasetExt);

        const report = await shaclService.validate(shape, data)
        checkNumberOfViolations(report, 4)

        test_min_vio(report, EX_PREFIX + 'myTermsAndConditionsMinVio', GX_PREFIX + 'url')
        test_min_vio(report, EX_PREFIX + 'myTermsAndConditionsMinVio', GX_PREFIX + 'hash')

        test_max_vio(report, EX_PREFIX + 'myTermsAndConditionsMaxVio', GX_PREFIX + 'url')
        test_max_vio(report, EX_PREFIX + 'myTermsAndConditionsMaxVio', GX_PREFIX + 'hash')
    });

    test("Wrong attributes values", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/terms-and-conditions-wrong-values.json');

        expect(shape).not.toBeNull();
        expect(shape).toBeInstanceOf(DatasetExt);

        const report = await shaclService.validate(shape, data)
        checkNumberOfViolations(report, 2)

        test_data_type(report, EX_PREFIX + 'myTermsAndConditions', GX_PREFIX + 'url', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_ANY_URI)
        test_data_type(report, EX_PREFIX + 'myTermsAndConditions', GX_PREFIX + 'hash', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING)
    });

    test("Valid instance", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/terms-and-conditions-valid.json');
        await testValidInstance(data, shape, shaclService)
    });
});

describe("Data Account Export Test Shape", () => {
    test("Wrong attributes cardinality", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/data-export-account-wrong-card.json');

        expect(shape).not.toBeNull();
        expect(shape).toBeInstanceOf(DatasetExt);

        const report = await shaclService.validate(shape, data)
        checkNumberOfViolations(report, 6)

        test_min_vio(report, EX_PREFIX + 'myDataAccountExportMinVio', GX_PREFIX + 'requestType')
        test_min_vio(report, EX_PREFIX + 'myDataAccountExportMinVio', GX_PREFIX + 'accessType')
        test_min_vio(report, EX_PREFIX + 'myDataAccountExportMinVio', GX_PREFIX + 'formatType')

        test_max_vio(report, EX_PREFIX + 'myDataAccountExportMaxVio', GX_PREFIX + 'requestType')
        test_max_vio(report, EX_PREFIX + 'myDataAccountExportMaxVio', GX_PREFIX + 'accessType')
        test_max_vio(report, EX_PREFIX + 'myDataAccountExportMaxVio', GX_PREFIX + 'formatType')
    });

    test("Wrong attributes values", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/data-export-account-wrong-values.json');

        expect(shape).not.toBeNull();
        expect(shape).toBeInstanceOf(DatasetExt);

        const report = await shaclService.validate(shape, data)
        checkNumberOfViolations(report, 3)

        test_data_type(report, EX_PREFIX + 'myDataAccountExport', GX_PREFIX + 'requestType', IN_CONSTRAINT, '')
        test_data_type(report, EX_PREFIX + 'myDataAccountExport', GX_PREFIX + 'accessType', IN_CONSTRAINT, '')
        test_data_type(report, EX_PREFIX + 'myDataAccountExport', GX_PREFIX + 'formatType', IN_CONSTRAINT, '')
    });

    test("Valid instance", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/data-export-account-valid.json');
        await testValidInstance(data, shape, shaclService)
    });
});


