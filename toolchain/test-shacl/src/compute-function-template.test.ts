import {beforeAll, describe, expect, test} from "vitest";
import {ShaclService} from "./ShaclService";
import DatasetExt from 'rdf-ext/lib/Dataset';

import {
    testValidInstance,
    EX_PREFIX,
    GX_PREFIX,
//    test_number_of_violations,
    checkNumberOfViolations,
    test_min_vio,
    test_max_vio,
    test_data_type,
    CLASS_CONSTRAINT,
    IN_CONSTRAINT,
    OR_CONSTRAINT,
    ERR_MESSAGE_NOT_OF_TYPE_INTEGER,
    ERR_MESSAGE_NOT_OF_TYPE_STRING,
    DATA_TYPE_CONSTRAINT
} from "../common/utils";

let shaclService: ShaclService;
shaclService = new ShaclService();

let shape: DatasetExt;

beforeAll(async () => {
    //shape = await shaclService.loadFromTurtleFile("../../gaia-x/shacl/gaia-x.shacl.ttl");
    shape = await shaclService.loadFromTurtleFile("../../shapes.shacl.ttl");
});


describe("Compute Function Template Shapes", () => {
    test('Check loaded shape', async () => {
        expect(shape).not.toBeNull();
        expect(shape).toBeInstanceOf(DatasetExt);
    });

    test("Valid instance", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/compute-function-template-valid.json');
        await testValidInstance(data, shape, shaclService)
    });

    test("Wrong attributes cardinality", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/compute-function-template-wrong-card.json')

        expect(shape).not.toBeNull();
        expect(shape).toBeInstanceOf(DatasetExt);
        const report = await shaclService.validate(shape, data)
        checkNumberOfViolations(report,3)

        //  Test count of attributes in ComputeFunctionTemplate Shape
        test_min_vio(report, EX_PREFIX + 'myWrongTemplate', GX_PREFIX + 'computeFunctionName')
        test_min_vio(report, EX_PREFIX + 'myWrongTemplate', GX_PREFIX + 'computeFunctionDescription')
        test_min_vio(report, EX_PREFIX + 'myWrongTemplate', GX_PREFIX + 'computeFunctionTemplateRuntime')
    });

    test("Wrong attributes values", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/compute-function-template-wrong-values.json');

        expect(shape).not.toBeNull();
        expect(shape).toBeInstanceOf(DatasetExt);
        const report = await shaclService.validate(shape, data)
        checkNumberOfViolations(report,7)

        // test wrong values in ComputeFunctionTemplate Shape
        test_data_type(report, EX_PREFIX + 'myWrongTemplate', GX_PREFIX + 'copyrightOwnedBy', OR_CONSTRAINT, "")
        test_data_type(report, EX_PREFIX + 'myWrongTemplate', GX_PREFIX + 'license', OR_CONSTRAINT, "")
        test_data_type(report, EX_PREFIX + 'myWrongTemplate', GX_PREFIX + 'resourcePolicy', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING)
        test_data_type(report, EX_PREFIX + 'myWrongTemplate', GX_PREFIX + 'computeFunctionName', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING)
        test_data_type(report, EX_PREFIX + 'myWrongTemplate', GX_PREFIX + 'computeFunctionDescription', DATA_TYPE_CONSTRAINT, ERR_MESSAGE_NOT_OF_TYPE_STRING)
        test_data_type(report, EX_PREFIX + 'myWrongTemplate', GX_PREFIX + 'computeFunctionTemplateRuntime', CLASS_CONSTRAINT, "")
    });    
});
