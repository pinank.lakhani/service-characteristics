import {beforeAll, describe, expect, test} from "vitest";
import {ShaclService} from "./ShaclService";
import DatasetExt from 'rdf-ext/lib/Dataset';

import {
    checkNumberOfViolations,
    CLASS_CONSTRAINT,
    EX_PREFIX,
    GX_PREFIX,
    test_data_type,
    test_max_vio,
    testValidInstance
} from "../common/utils";

let shaclService: ShaclService;
shaclService = new ShaclService();

let shape: DatasetExt;

beforeAll(async () => {
    shape = await shaclService.loadFromTurtleFile("../../shapes.shacl.ttl");
});

describe("Service offering QoS Shapes", () => {
    test('Check loaded shape', async () => {
        expect(shape).not.toBeNull();
        expect(shape).toBeInstanceOf(DatasetExt);
    });

    test("Wrong attributes cardinality", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/service-offering-qos-wrong-card.json');
        expect(shape).not.toBeNull();
        expect(shape).toBeInstanceOf(DatasetExt);

        const report = await shaclService.validate(shape, data)
        checkNumberOfViolations(report, 9)
        
        test_max_vio(report, EX_PREFIX + 'myQoSWrongCard', GX_PREFIX + 'bandWidth')
        test_max_vio(report, EX_PREFIX + 'myQoSWrongCard', GX_PREFIX + 'roundTripTime')
        test_max_vio(report, EX_PREFIX + 'myQoSWrongCard', GX_PREFIX + 'availability')
        test_max_vio(report, EX_PREFIX + 'myQoSWrongCard', GX_PREFIX + 'packetLoss')
        test_max_vio(report, EX_PREFIX + 'myQoSWrongCard', GX_PREFIX + 'jitter')
        test_max_vio(report, EX_PREFIX + 'myQoSWrongCard', GX_PREFIX + 'latency')
        test_max_vio(report, EX_PREFIX + 'myQoSWrongCard', GX_PREFIX + 'targetPercentile')
        test_max_vio(report, EX_PREFIX + 'myQoSWrongCard', GX_PREFIX + 'throughput')
        test_max_vio(report, EX_PREFIX + 'myQoSWrongCard', GX_PREFIX + 'iops')        
    });
    
    test("Wrong attributes values", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/service-offering-qos-wrong-values.json');                                       
        expect(shape).not.toBeNull();
        expect(shape).toBeInstanceOf(DatasetExt);                                      

        const report = await shaclService.validate(shape, data)
        checkNumberOfViolations(report, 18)

        test_data_type(report, EX_PREFIX + 'myQoSWrongValues', GX_PREFIX + 'bandWidth', CLASS_CONSTRAINT, '')
        test_data_type(report, EX_PREFIX + 'myQoSWrongValues', GX_PREFIX + 'roundTripTime', CLASS_CONSTRAINT, '')
        test_data_type(report, EX_PREFIX + 'myQoSWrongValues', GX_PREFIX + 'availability', CLASS_CONSTRAINT, '')
        test_data_type(report, EX_PREFIX + 'myQoSWrongValues', GX_PREFIX + 'packetLoss', CLASS_CONSTRAINT, '')
        test_data_type(report, EX_PREFIX + 'myQoSWrongValues', GX_PREFIX + 'jitter', CLASS_CONSTRAINT, '')
        test_data_type(report, EX_PREFIX + 'myQoSWrongValues', GX_PREFIX + 'latency', CLASS_CONSTRAINT, '')
        test_data_type(report, EX_PREFIX + 'myQoSWrongValues', GX_PREFIX + 'targetPercentile', CLASS_CONSTRAINT, '')
        test_data_type(report, EX_PREFIX + 'myQoSWrongValues', GX_PREFIX + 'throughput', CLASS_CONSTRAINT, '')
        test_data_type(report, EX_PREFIX + 'myQoSWrongValues', GX_PREFIX + 'iops', CLASS_CONSTRAINT, '')            
    });

    test("Valid instance", async () => {
        let data = await shaclService.loadFromJsonLDFile('data/service-offering-qos-valid.json');
        await testValidInstance(data, shape, shaclService)
    });    
});
