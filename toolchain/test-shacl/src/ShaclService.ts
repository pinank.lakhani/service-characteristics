import DatasetExt from 'rdf-ext/lib/Dataset'
import ParserN3 from '@rdfjs/parser-n3'
import rdf from 'rdf-ext'
import ParserJsonLD from '@rdfjs/parser-jsonld'
import {Readable} from 'stream'
import fs from 'fs'
import SHACLValidator from 'rdf-validate-shacl';

export class ShaclService {

    async validate(shape: DatasetExt, data: DatasetExt) {
        const validator: SHACLValidator = new SHACLValidator(shape, {factory: rdf})
        return validator.validate(data);
    }

    async loadFromTurtleFile(filename: string): Promise<DatasetExt> {
        try {
            const parser = new ParserN3({factory: rdf as any})
            const stream = fs.createReadStream(filename);
            return await rdf.dataset().import(parser.import(stream));
        } catch (error) {
            console.error(error);
            throw new Error('Cannot load from provided turtle.')
        }
    }

    async loadFromTurtle(raw: string): Promise<DatasetExt> {
        try {
            const parser = new ParserN3({factory: rdf as any})
            return this.transformToStream(raw, parser)
        } catch (error) {
            console.error(error);
            throw new Error('Cannot load from provided turtle.')
        }
    }

    async loadFromJsonLD(raw: string): Promise<DatasetExt> {
        try {
            const parser = new ParserJsonLD({factory: rdf})
            return this.transformToStream(raw, parser)
        } catch (error) {
            console.error(error)
            throw new Error('Cannot load from provided JsonLD.')
        }
    }

    async loadFromJsonLDFile(filename: string): Promise<DatasetExt> {
        try {
            const parser = new ParserJsonLD({factory: rdf})
            const stream = fs.createReadStream(filename);
            return await rdf.dataset().import(parser.import(stream));
        } catch (error) {
            console.error(error)
            throw new Error('Cannot load from provided JsonLD.')
        }
    }

    async merge(data1: DatasetExt, data2: DatasetExt) {
        for (const quad of data2) {
            data1.add(quad);
        }
        return data1;
    }

    private async transformToStream(raw: string, parser: any): Promise<DatasetExt> {
        const stream = new Readable()
        stream.push(raw)
        stream.push(null)
        return await rdf.dataset().import(parser.import(stream))
    }
}
