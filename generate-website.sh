#!/bin/sh

echo "-> Clearing website/docs/ontology folder..."
rm -rf website/docs/ontology
mkdir website/docs/ontology

echo "-> Generating ontology documentation..."
for file in ./single-point-of-truth/*.yaml; do
  filename=$(basename "$file")
  filename_without_extension=$(echo "$filename" | cut -d. -f1)

  if [[ $filename_without_extension == 'gaia-x' ]]; then
    continue
  fi

  gen-doc --no-mergeimports --template-directory website/templates/ -d website/docs/ontology/"${filename_without_extension}" "$file"
done

echo "-> Copying README.md file as homepage..."
cp -f README.md website/docs/index.md