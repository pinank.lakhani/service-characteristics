id: https://w3id.org/gaia-x/ONTOLOGY_VERSION#connectivity-configuration
name: connectivity-configuration
prefixes:
  gx: https://w3id.org/gaia-x/ONTOLOGY_VERSION#
  linkml: https://w3id.org/linkml/
imports:
  - linkml:types
  - instantiation-requirement
  - resource
  - datacenter
default_prefix: gx
default_range: string

classes:
  ConnectivityConfiguration:
    is_a: InstantiationRequirement
    title: 'Connectivity Configuration'
    description: 'The connectivityConfiguration class encapsulates the configurable attributes of a ConnectivityServiceOffering.'
    attributes:
      sourceIdentifierA:
        title: 'source identifier'
        description: 'InterconnectionPointIdentifier reference of the source service access point.'
        multivalued: false
        any_of:
          - range: PhysicalInterconnectionPointIdentifier
          - range: VirtualInterconnectionPointIdentifier
      destinationIdentifierZ:
        title: 'destination identifier'
        description: 'InterconnectionPointIdentifier reference of the destination service access point.'
        multivalued: false
        any_of:
          - range: PhysicalInterconnectionPointIdentifier
          - range: VirtualInterconnectionPointIdentifier

  PhysicalInterconnectionPointIdentifier:
    is_a: PhysicalResource
    title: 'physical interconnection point identifier'
    description: 'TBD'
    attributes:
      interconnectionPointIdentifier:
        title: 'interconnection point identifier'
        description: 'Definition of the location where resources can interconnect.'
        required: true
        multivalued: false
        range: InterconnectionPointIdentifier    

  VirtualInterconnectionPointIdentifier:    
    is_a: VirtualResource
    title: 'virtual interconnection point identifier'
    description: 'TBD'
    attributes:
      interconnectionPointIdentifier:
        title: 'interconnection point identifier'
        description: 'Definition of the location where resources can interconnect.'
        required: true
        multivalued: false
        range: InterconnectionPointIdentifier        

  InterconnectionPointIdentifier:
    title: 'Interconnection Point Identifier'
    description: 'Interconnection Point Identifier is a unique Service Access Point that identifies the location where resources can interconnect.'
    attributes:
      ipiType:
        title: 'ipi type'
        description: 'Type of the interconnection point identifier.'
        range: IPIType
        ifabsent: string(Other)
      ipiProvider:
        title: 'ipi provider'
        description: 'The Provider of the IPI.'
        range: string
      specificParameters:
        title: 'specific parameters'
        description: 'Type or Provider Specific Parameters, separated by colons.'
        range: string
      completeIPI:
        title: 'complete IPI'
        description: 'Unique label identifying structure of an IPI.'
        identifier: true
        required: true
        string_serialization: '{ipiType}:{ipiProvider}:{specificParameters}'
      datacenterAllocation:
        title: 'datacenter allocation'
        description: 'Details specific situation within the datacenter where the service can be accessed.'
        multivalued: false
        range: DatacenterAllocation
      macAddress:
        title: 'mac address'
        description: 'The mac address required for L2 connectivity setup.'
        multivalued: false
        range: string
        pattern: '(?:[0-9a-fA-F]{2}\:){5}[0-9a-fA-F]{2}'
      ipAddress:
        title: 'ip address'
        description: 'The IP address required for L3 connectivity setup.'
        multivalued: false
        range: string
        pattern: '\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}'

  DatacenterAllocation:
    title: 'datacenter allocation'
    description: 'Details specific situation within the datacenter where the service can be accessed.'
    attributes:
      refersTo:
        title: 'refers to'
        description: 'Datacenter where the service can be accessed.'
        required: true
        multivalued: false
        range: Datacenter
      floor:
        title: 'datacenter floor'
        description: 'The floor number of the datacenter where the service can be accessed.'
        multivalued: false
        range: string
      rackNumber:
        title: 'datacenter rack number'
        description: 'The Id of the datacenter rack number where the service can be accessed.'
        multivalued: false
        range: string
      patchPanel:
        title: 'datacenter patch panel'
        description: 'The Id of the datacenter patch panel where the service can be accessed.'
        multivalued: false
        range: string
      portNumber:
        title: 'datacenter port number'
        description: 'The port number on the patch panel where the service can be accessed.'
        multivalued: false
        range: integer

  VLANConfiguration:
    title: 'VLAN configuration'
    description: 'TDB'
    attributes:
      vlanType:
        title: 'vlan type'
        description: 'The chosen types of vlan types.'
        multivalued: false
        range: VlanType
      vlanTag:
        title: 'vlan tag'
        description: 'Vlan Tag ID that range between 1 and 4094. In case qinq connection type is chosen tow vlan tag, namely outer and innter should be provided.'
        multivalued: false
        range: integer
        minimum_value: 1
        maximum_value: 4094
      vlanEtherType:
        title: 'vlan ether type'
        description: 'The ether type of the vlan in hexadecimal notation.'
        multivalued: false
        range: string    

enums:
  IPIType:
    permissible_values:
      Physical:
        description: 'TBD'
      Link:
        description: 'TBD'
      URI:
        description: 'TBD'
      Network:
        description: 'TBD'
      Other:
        description: 'TBD'
        
  VlanType:
    permissible_values:
      qinq:
        description: 'TBD'
      dot1q:
        description: 'TBD'
      other:
        description: 'TBD'          
