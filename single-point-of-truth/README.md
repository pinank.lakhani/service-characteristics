# Single Point of Truth

The Single Point of Truth (SPoT) contains the singular, main source of data for the
entire Service Characteristics project and therefore for all "officially" provided Self-Descriptions in Gaia-X.

The SPoT provides YAML files in [linkml](https://linkml.io/) format as its primary information source, which contain specific classes with their attributes
and respective information like attributes' datatypes or cardinalities. 

These YAML files are then used in the CI process to create all the necessary files, that are used for the
Self-Descriptions and the processes connected with them. 

CI/CD Pipeline works as follows:

- building SD artefacts using [linkml](https://linkml.io/): 
   - SHACL shapes to validate claims in Gaia-X credential instances
   - OWL ontology for Gaia-X credentials
   - JSON-LD context
- test generates shacl files using unit tests in [test-shacl](../toolchain/test-shacl)
- validate Self-Description instances in [instances](../instances)   

The SPoT should be updated regularly in order to reflect the current needs of the real-world and therefore the Gaia-X
participants.

LinkML has some bugs, currently, which needs to be fixed upstream. For now, you should our  [linkML fork](https://github.com/anjastrunk/linkml.git), where these bugs are already fixed. 
## Documentations

- [Gaia-X Trust Framework](https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/)
- [Gaia-X Architecture model](https://gaia-x.gitlab.io/technical-committee/architecture-document/)

