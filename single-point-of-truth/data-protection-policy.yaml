id: https://w3id.org/gaia-x/ONTOLOGY_VERSION#data-protection-policy
name: data-protection-policy
prefixes:
  gx: https://w3id.org/gaia-x/ONTOLOGY_VERSION#
  linkml: https://w3id.org/linkml/
  qudt: http://qudt.org/vocab/

imports:
  - linkml:types
  - address
  - quantity
  - resource
  
default_prefix: gx
default_range: string

classes:
  DataProtectionPolicy:    
    description: "A container class for various data protection features, such as backup or replication"
    abstract: true
    attributes:
        protectionFrequency: 
            title: "Protection Frequency"
            description: "Frequency at which data are captured/protected."
            required: true
            range: ProtectionFrequency
        protectionRetention:
            title: "Protection Retention"
            description: "How long captured/protected data are kept available."
            required: true
            range: RetentionDuration
        protectionMethod:
            title: "Protection Method"
            description: "Method used to protect data, can be be full copy, incremental,..."
            range: ProtectionMethod

  BackupPolicy:
    description: "Describe data protection features based on Backup for storage services"
    is_a: DataProtectionPolicy
    attributes:
      backupLocation:
        title: "Backup Location"
        description: "Where are located data backups"
        range: Resource 
        required: true
        multivalued: true
      backupReplication:
        title: "Backup Replication"
        description: "Backups replication policy, if any"
        range: ReplicationPolicy  
        multivalued: true

  SnapshotPolicy:
    description: "Describe data protection features based on Snapshot for storage services"
    is_a: DataProtectionPolicy
    attributes:
      snapshotReplication:
        title: "Snapshot Replication"
        description: "Snapshots replication policy, if any"
        range: ReplicationPolicy  
        multivalued: true

  ReplicationPolicy:
    description: "Describe data protection features based on Replication for storage services"
    is_a: DataProtectionPolicy
    attributes:
      synchronousReplication:
        title: "Synchronous Replication"
        description: "Whether replication is performed in synchronous mode"
        range: boolean
      consistencyType:
        title: "Consistency Level"
        description: "Type of consistency supported"
        range: ConsistencyType
      replicaNumber:
        title: "Replica Number"
        description: "How many independant copies are made"
        range: integer
        multivalued: true
      geoReplication:
        title: "Geo Replication"
        description: "Scope of geo-replication, cross-region, cross-dc or simply cross-az"
        range: GeoReplicationScope
        multivalued: true
      
enums:
  ProtectionFrequency:
    permissible_values:
      Hourly:
        description: "For describing protection policies triggered once an hour"
      Daily:
        description: "For describing protection policies triggered once a day"
      Weekly:
        description: "For describing protection policies triggered once a week"
      Monthly:
        description: "For describing protection policies triggered once a month"
      Continuously:
        description: "For describing protection policies triggered continuously, ie each time data are modified"
      OnDemand:
        description: "For describing unplanned protection policies triggered on demand"

  ProtectionMethod:
    permissible_values:
      Full-Copy:
        description: "For describing protection through full copy of protected data"
      Incremental:
        description: "For describing protection through differential copy of protected data, ie only modifications are copied"
  GeoReplicationScope:
    permissible_values:
      Cross-Region:
        description: "Whenever replication is span across multiple regions"
      Cross-DC:
        description: "Whenever replication is span across multiple datacenters within a region"
      Cross-AZ:
        description: "Whenever replication is span across multiple availability zones"
  ConsistencyType:
    permissible_values:
      Strong:
        description: "Strong consistency model, cf https://en.wikipedia.org/wiki/Consistency_model#Strong_consistency_models."
      Session:
        description: "Eventual consistency model, cf https://en.wikipedia.org/wiki/Consistency_model#Session_guarantees."  
      Eventual:
        description: "Eventual consistency model, cf https://en.wikipedia.org/wiki/Eventual_consistency."
      Bounded-Staleness:
        description: "Consistency model where the lag between replicas is bounded, either by a number of updates or a time limit."
      Other:
        description: "Unspecified consistency model."
        
  
  