id: https://w3id.org/gaia-x/ONTOLOGY_VERSION#memory
name: memory
prefixes:
  gx: https://w3id.org/gaia-x/ONTOLOGY_VERSION#
  linkml: https://w3id.org/linkml/
imports:
  - linkml:types
  - quantity
  - device
default_prefix: gx
default_range: string

classes:
  Memory:
    is_a: Device
    description: Details with respect to properties and capabilities of RAM.
    attributes:
      memorySize:
        title: "memory size"
        description: "Memory size of RAM."
        range: MemorySize
        required: true
      memoryClass:
        title: "memory class"
        description: "DRAM technology name defined by JEDEC."
        range: MemoryClasses
        ifabsent: string(other)
      memoryRank:
        title: "memory rank"
        description: "Rank defines the number of memory chip banks and the buffering used to access these."
        range: MemoryRanks
        ifabsent: string(other)
      eccEnabled:
        title: "error correction code (ecc)"
        description: "Error connection code allows to detect and correct bit errors in data. True, if error-correction-code is supported by the ram, false otherwise. Default: False."
        ifabsent: False
        range: boolean
      hardwareEncryption:
        title: "Hardware Encryption enabled"
        description: "If true, encryption of memory at the hardware level is enabled. Default: False."
        range: boolean
        ifabsent: False

enums:
  MemoryClasses:
    permissible_values:
      SDRAM:
        description: "Synchronous DRAM, improves performance in data transfer actions through its pins."
      DDR SDRAM:
        description: "SDRAM evolution with twice the data transmission frequency."
      ECC DRAM:
        description: "This type of DRAM memory can find and even fix corrupted data."
      DDR4:
        description: "Fourth generation of the synchronous dynamic random-access memory interface, improves speed and efficiency in data transfer actions."
      DDR5:
        description: "Fifth generation of the synchronous dynamic random-access memory interface, planned to reduce power consumption."
      GDDR5:
        description: "Type of memory that is also based on the DDR standard, but designed specifically to be used in graphic cards."
      GDDR6:
        description: "Evolution of GDDR5 memory, designed for high-performance computing."
      other:
        description: "Memory class no further described."

  MemoryRanks:
    permissible_values:
      1R RDIMM:
        description: "(Registered DIMM) A single-rank (1R) memory module contains one set of DRAM chips that is accessed during writing/reading operations."
      2R RDIMM:
        description: "(Registered DIMM) A dual-rank (2R) memory module contains two sets of DRAM chips that are accessed during writing/reading operations."
      4R LRDIMM:
        description: "(Load Reduced DIMM) A quad-rank (4R) memory module contains four sets of DRAM chips that are accessed during writing/reading operations."
      other:
        description: "Memory rank no further described."
