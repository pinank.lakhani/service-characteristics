id: https://w3id.org/gaia-x/ONTOLOGY_VERSION#cryptography
name: cryptography
prefixes:
  gx: https://w3id.org/gaia-x/ONTOLOGY_VERSION#
  linkml: https://w3id.org/linkml/
imports:
  - linkml:types
default_prefix: gx
default_range:  string

classes:
  Encryption:
    description: "Encryption capabilities of a Gaia-X entity."
    attributes:
      cipher:
        title: "encryption algorithm"
        description: "Supported algorithm used to encrypt."
        range: EncryptionAlgorithm
        required: true
      keyManagement:
        title: "key management"
        description: "Define key management method. Allowed values are: managed, BYOK, HYOK"
        range: KeyManagement
        required: true

  CheckSum:
    description: "Detail on how to calculate or verify a checksum."
    attributes:
      checkSumCalculation:
        description: "Algorithm used to create checksum."
        range: ChecksumAlgorithm
        required: true
      checkSumValue:
        description: "Value of the check sum."
        range: string
        required: true

  Signature:
    attributes:
      signatureValue:
        description: 'Value of the signature'
        range: string
        required: true
      hashAlgorithm:
        description: "Algorithm used to create hash."
        range: ChecksumAlgorithm
        required: true
      signatureAlgorithm:
        description: 'Defines the algorithm used to calculate or verify the signature.'
        range: SignatureAlgorithm
        required: true


enums:
  EncryptionAlgorithm:
    permissible_values:
      RSA:
        description: "Algorithm for encryption based on public key cryptography (asymmetric cryptography), which uses two keys (public and private) for encoding and sending messages and data."
      AES:
        description: "(Advanced Encryption Standard) Algorithm for encryption based on symmetric cryptography (secret key cryptography) that works over group of bits of fixed length (blocks). "
      3DES:
        description: "(Triple Data Encryption Algorithm) Symmetric-key block cipher algorithm for encryption, which applies three times the DES (Data Encryption Standard) algorithm to each single data block."
      Blowfish:
        description: "Symmetric-key block cipher algorithm for encryption that includes advanced functionalities aimed to improve DES algorithm."
      Twofish:
        description: "Evolution of the Symmetric-key block cipher Blowfish algorithm."
      SDA:
        description: "(Static Data Authentication) is a digital signature scheme that works with asymmetric cryptography."
      other:
        description: "Algorithm for encryption not further described."

  ChecksumAlgorithm:
    permissible_values:
      md5:
        description: "(Message Digest Method 5) Algorithm that generates a 128-bit digest, represented as 32-digit hexadecimal numbers, regardless the length of the input string."
      sha-1:
        description: "(Secure Hash Algorithm 1) Algorithm that generates a 160-bit digest, typically represented as 40-digit hexadecimal numbers, regardless the length of the input string."
      sha-256:
        description: "Cryptographic algorithm that produces a fixed-length, 256-bit (32-byte) hash value. The input text should be less than 264 bits length. The algorithm is irreversible by design."
      sha-384:
        description: "This variant converts text of any length into a fixed-size string. Each output produces a hash of 384 bits."
      sha-512:
        description: "This variant converts text of any length into a fixed-size string. Each output produces a hash of 512 bits (64 bytes)."
      sha-3:
        description: "Advanced version of the sha function, based on a sponge function approach, that generates an output bit stream of a desired length (56, 64, 96 or 128-digit hexadecimal numbers) depending of the sha-3 function version used."
      ripemd-160:
        description: "RIPEMD-160 is a hash function from the RIPEMD (RIPE Message Digest) family that generates 160-bit hashes represented as 40-digit hexadecimal numbers."
      blake2:
        description: "Improved version of the sha-3 algorithm Blake, optimized for 64-bit platforms, that generates digests of size between 1 and 64 bytes (Blake2b) or up to 32 bytes (Blake2s)."
      blake3:
        description: "Evolution of Bao and Blake2, although in contrast to these, doesn't offer different variants depending on the architectures, but just a single algorithm."
      other:
        description: "Algorithm to calculate checksum not further described."

  KeyManagement:
    permissible_values:
      BYOK:
        description: "bring-your-own-key: Keys created by user and stored in key manager of cloud"
      HYOK:
        description: "hold-your-own-key Key created by user and kept by user"
      managed:
        description: "managed: Keys are created by and stored in key manager of cloud."

  SignatureAlgorithm:
    permissible_values:
      RSA-Signature:
        description: "RSA is a cryptographic system based on public key that can be used both for signing and encryption purposes."
      DSA:
        description: "DSA (Digital Signature Algorithm) is an asymmetric algorithm for signing information proposed by the NIST (National Institute of Standards and Technology)."
      KCDSA:
        description: "(Korean Certificate-based Digital Signature Algorithm), created by the Korea Internet & Security Agency (KISA), is similar to the DSA algorithm and can produce and output from 128 to 256 bits, in 32-bit increments."
      Schnorr:
        description: "Similar to ECDSA scheme was described by Claus Schnorr, is a well-known algorithm due to its simplicity and efficiency that generates short signatures."
      ECDSA:
        description: "ECDSA (Elliptic Curve Digital Signature Algorithm) is an improved variant of the DSA algorithm which uses keys derived from elliptic curve cryptography."
      ECKDSA:
        description: "Elliptic Curve variant of the KCDSA algorithm."
      EC Schnorr:
        description: "Elliptic Curve Based Schnorr signatura algorithm."
      ECGDSA:
        description: "(Elliptic Curve German Digital Signature Algorithm), variant of the ECDSA algorithm."
      other:
        description: "Algorithm for digital signatures not further described."
