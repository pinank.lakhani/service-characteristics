id: https://w3id.org/gaia-x/ONTOLOGY_VERSION#image
name: image
prefixes:
  gx: https://w3id.org/gaia-x/ONTOLOGY_VERSION#
  linkml: https://w3id.org/linkml/
imports:
  - cpu
  - gpu
  - disk
  - memory
  - quantity
  - resource
  - code-artifact
  #- server-flavor
default_prefix: gx
default_range: string

classes:
  Image:
    title: "Image"
    description: "A software piece that can be executed by a virtual or bare metal Compute services. It is a subclass of OperatingSystem and CodeArtifact."
    is_a: CodeArtifact
    abstract: true
    attributes:
      fileSize:
        title: "file size"
        description: Size of the image.
        range: MemorySize
      operatingSystem:
        title: "Operating System"
        description: A resolvable link to Gaia-X credential of operation system offered by this image.
        range: OperatingSystem
      cpuReq:
        title: "CPU requirements"
        description: Details with respect to CPU capabilities required to run the image.
        range: CPU
      gpuReq:
        title: "GPU requirements"
        description: Details with respect to GPU capabilities required to run the image.
        range: GPU
      ramReq:
        title: "Ram requirements"
        description: Minimal size of RAM required to run the image.
        range: Memory
      videoRamSize:
        title: "Video RAM size"
        description: Maximum amount of RAM for the video image.
        range: MemorySize
      rootDiskReq:
        title: "root disk requirements"
        description: Minimal size of root disk required to run the image.
        range: Disk
      encryption:
        description: Details with respect to encryption of the images.
        range: Encryption
      checkSum:
        description: Details with respect to check sum of the image.
        range: CheckSum
      secureBoot:
        title: "Secure Boot"
        description: "If true, instances of the image are only started, if signature of software, such as firmware or operating system, are valid. Default: False."
        range: boolean
        ifabsent: False
      vPMU:
        title: "Virtual performance monitoring unit"
        description: "If true, a virtual performance monitoring unit (vPMU) is enable in guest. Default: False"
        range: boolean
        ifabsent: False
      multiQueues:
        title: "Multi queues"
        description: If true, one queue is set for each virtual CPU.
        range: boolean
        ifabsent: False
      updateStrategy:
        description: Details on provider's image update strategy of this image. An update strategy defines ...
        range:   UpdateStrategy
      licenseIncluded:
        title: "License included"
        description: "In case an image requires a commercial license to be used (No Freeware), this attribute indicates, if service usage fee includes that license (true) or customer has to bring its own commercial license (False). Default: False"
        range: boolean
        ifabsent: False
      maintenance:
        description: "Details on maintenance capabilities of vendor of image's operating system."
        range: MaintenanceSubscription

  MaintenanceSubscription:
    description: A maintenance subscriptions gives access to bug fixes, security fixes and function updates from software vendor.
    attributes:
      subscriptionIncluded:
        title: "Subscription included"
        description: "True, if cloud  service provider prepared already the image to reviece bug fixes, security fixes and (minor) function updates. Default: False"
        range: boolean
        ifabsent: False
      subscriptionRequired:
        title: "Subscription required"
        description: "True, if cloud service user needs a maintenance subscription account from the vendor of image's operating system in order to receive fixes. Default: False."
        range: boolean
        ifabsent: False
      maintainedUntil:
        title: "Maintenance until"
        description: Date (UTC) until vendor of image's operating system promises maintenance at least.
        range: date

  UpdateStrategy:
    description: Cloud service customer expect cloud images to be updated regularly, in order to always get the image with the lasted patches. Technically, an updated image is a new image in cloud service' image catalogue having it's own unique image ID and replaces its old version. It is recommended to hide outdated version, but keep them referencable by its ID for a transition period for customers' convenience. This class defines important aspects of providers image update policy.
    attributes:
      replaceFrequency:
        title: "Replace Frequency"
        description: "Frequency, in which the provider updates the image on a regular basis. Possible values are: yearly, quarterly, monthly, weekly, daily, critical_bug, and never."
        range: UpdateFrequency
      hotfixHours:
        title: "Hotfix hours"
        description: Time in hours passed after image's distributor published a critical hot fix and cloud provider updated this image with the critical patch. A critical bug is defined as a security vulnerability with a CVSS score of 9.0 or higher.
        range: integer
        minimum_value: 0
      oldVersionsValidUntil:
        title: "Old versions Valid until"
        description: Defines, how long outdated and hidden images are available by its ID. Allowed values are "none" (none information are given), "notice" (outdated version is valid until deprecation notice from provider), "forever" (Outdated version will never be deleted), non-negative integer n (latest n version keep available) and date in UTC (outdated version is valid until given date).
        any_of:
          - range: Validity_1
          - range: Validity_2
          - range: Latest_N
          - range: date
      providedUntil:
        title: "Provided until"
        description: Details, how long the image will be provided in image catalogue. Allowed values are "none" (none information are given), "notice" (image is available until deprecation notice from provider), and date in UTC (date until the image is available).
        any_of:
          - range: Validity_1
          - range: date
      
  Latest_N:
    description: Number of latest N outdated image versions, which will be valid.
    attributes:
      value:
        range: integer
        minimum_value: 1


enums:
  UpdateFrequency:
      description: Possible values for image's update frequency.
      permissible_values:
        yearly:
          description: Image will be updated at least once per year.
        quarterly:
          description: Image will be updated at least once per quarter.
        monthly:
          description: Image will be updated at least once per month.
        weekly:
          description: Image will be updated at least once per week.
        daily:
          description: Image will be updated at least once per day.
        critical_bug:
          description: Image will be updated for critical bugs only.
        never:
          description: Image will never be updated.

  Validity_1:
    description: Possible values for definition of image's validity after upgrading to a new version.
    permissible_values:
      none:
        description: No information are given.
      notice:
        description: Outdated version of the image will remain valid until a deprecation notice will be published.

  Validity_2:
    description: Possible values for definition of image's validity after upgrading to a new version.
    permissible_values:
      forever:
        description: Outdated version of the image will remain valid for as long as the cloud operates.
